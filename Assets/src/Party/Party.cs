﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Party : MonoBehaviour {

	public static String kPartyStringKey = "party";

	public enum PartyType {
		Player, Enemy, Neutral
	}

	private Board board;

	public List<Character> members;
	public ArrayList items;
	public bool isCPU;
	public PartyType type;



	// Use this for initialization
	void Awake () {
		members = new List<Character> ();
		items = new ArrayList ();
	}
	
	// Update is called once per frame
	public void Update () {
		for (int i = 0; members != null && i < members.Count; i++) {
			Character c = (Character)members [i];
			if (c.hp <= 0) {
				//c.getBoardSection ().removeCharacter ();
				members.Remove (c);
				c.gameObject.SetActive (false);

				if (this.type == PartyType.Enemy) {
					board.setDeadCharacterEnemy (c);
				} else {
					board.setDeadCharacterFriendly (c);
				}
			}
		}
	}

	// resets all 
	public void StartTurn() {
		for (int i = 0; i < members.Count; i++){
			Character c = (Character)members [i];
			c.doTurnStart ();
			c.ap = c.totalAp;
		}

		if (isCPU) {
			for (int i = 0; i < members.Count; i++){
				Character c = (Character)members [i];
				for (int j = 0; j < c.ap; j++) {
					DoCPUMoveForCharacter (c);
					c.ap--;
				}
			}
			board.DropBoardState ();
			board.EndTurn ();
		} else {

		}
	}

	public void EndTurn() {
		for (int i = 0; i < members.Count; i++){
			Character c = (Character)members [i];
			c.doTurnEnd ();
		}

	}

	int searchForIndexOfPlayerParty() {
		List<Character> members = board.playerParty.members;

		if (members.Count == 0)
			return -1;

		int maxIndex = -1;
		float maxValue = float.MinValue;

		for (int i = 0; i < members.Count; i++) {
			Character c = (Character)members [i];

			if (c.getBoardSection () == null)
				continue;

			float val = (c.totalHp - c.hp);

			if (c.immuneToDamage)
				val -= 10.0f;

			if (val > maxValue)
				maxIndex = i;
		}

		return maxIndex;
	}

	int countAdjacentWalls(int r, int c) {
		int sum = 0;

		int nr = r - 1;
		int nc = c;
		if (FooBaz.isInBounds (board.collision, nr, nc)) {
			if (board.collision [nr, nc] == 0) {
				sum++;
			}
		} else {
			sum++;
		}

		nr = r + 1;
		nc = c;
		if (FooBaz.isInBounds (board.collision, nr, nc)) {
			if (board.collision [nr, nc] == 0) {
				sum++;
			}
		} else {
			sum++;
		}

		nr = r;
		nc = c - 1;
		if (FooBaz.isInBounds (board.collision, nr, nc)) {
			if (board.collision [nr, nc] == 0) {
				sum++;
			}
		} else {
			sum++;
		}

		nr = r;
		nc = c + 1;
		if (FooBaz.isInBounds (board.collision, nr, nc)) {
			if (board.collision [nr, nc] == 0) {
				sum++;
			}
		} else {
			sum++;
		}

		return sum;
	}

	int calculateDistanceToOtherCharacters(int r, int c, Character currentCharacter) {
		int dist = 0;
		foreach (Character character in members) {
			if (character == currentCharacter) {
				continue;
			}
			dist += board.ManhattanDistance(r, c, character.getBoardSection().row, character.getBoardSection().col);
		}
		return dist;
	}

	void DoCPUMoveForCharacter (Character c) {
		List<BoardSection> possibleMoves = board.SetActiveStateForSection (c.getBoardSection(), true);
		ArrayList weights = new ArrayList ();
		float maxWeight = float.MinValue;
		int maxIndex = -1;

		int index = searchForIndexOfPlayerParty ();

		if (index == -1)
			return;

		int targetRow = (board.playerParty.members [index] as Character).getBoardSection().row;
		int targetCol = (board.playerParty.members [index] as Character).getBoardSection().col;
		for(int i = 0; i < possibleMoves.Count; i++) {
			BoardSection section = (BoardSection)possibleMoves [i];

			int hasCharacter = section.getCharacter() != null && (section.getCharacter().characterType == Character.CharacterType.NuetralNonPlayerCharacter || section.getCharacter().characterType == Character.CharacterType.PlayerCharacter) ? 1 : 0;
			float weight = -board.ManhattanDistance (targetRow, targetCol, section.row, section.col) + hasCharacter * 10 - 2 * calculateDistanceToOtherCharacters (section.row, section.col, c) - countAdjacentWalls(section.row, section.col);


			if (section == c.getBoardSection())
				weight = float.MinValue;

			if (weight >= maxWeight) {
				maxWeight = weight;
				maxIndex = i;
			}
		}

		if (maxIndex == -1)
			return;

		BoardSection move = (BoardSection)possibleMoves [maxIndex];
		if (move.getCharacter () != null && move.getCharacter () != c && move.getCharacter ().characterType != Character.CharacterType.EnemyNonPlayerCharacter) {
			NPoint p = board.MoveWithinAttackRangeOfMe (move);

			if (p == null) {
				possibleMoves.Remove (move);
				DoCPUMoveForCharacter (c, possibleMoves);
				return;
			}

			board.MoveToMe (board.boardSections [p.r, p.c]);
			board.DropBoardState ();
			board.SetActiveStateForSection (board.boardSections [p.r, p.c], false);
			board.AttackMe (move);
			board.DropBoardState ();
		} else if (move.getCharacter () == null) {
			board.MoveToMe (move);
			board.DropBoardState ();
		} else {
			possibleMoves.Remove (move);
			DoCPUMoveForCharacter (c, possibleMoves);
		}
	}

	float computeDistanceToOtherCharacters(Character c) {
		float val = 0;
		for (int i = 0; i < members.Count; i++) {
			Character other = (Character) members [i];

			val += board.ManhattanDistance (c.getBoardSection ().row, c.getBoardSection ().col, other.getBoardSection ().row, other.getBoardSection ().col);
		}

		return val;
	}

	void DoCPUMoveForCharacter(Character c, List<BoardSection> possibleMoves) {
		if (possibleMoves.Count == 0)
			return;
		ArrayList weights = new ArrayList ();
		float maxWeight = float.MinValue;
		int maxIndex = -1;

		int index = searchForIndexOfPlayerParty ();

		if (index == -1)
			return;

		int targetRow = (board.playerParty.members [index] as Character).getBoardSection().row;
		int targetCol = (board.playerParty.members [index] as Character).getBoardSection().col;
		for(int i = 0; i < possibleMoves.Count; i++) {
			BoardSection section = (BoardSection)possibleMoves [i];

			int hasCharacter = section.getCharacter() != null && (section.getCharacter().characterType == Character.CharacterType.NuetralNonPlayerCharacter || section.getCharacter().characterType == Character.CharacterType.PlayerCharacter) ? 1 : 0;
			float weight = -board.ManhattanDistance (targetRow, targetCol, section.row, section.col) + hasCharacter * 7.0f + 1.0f / computeDistanceToOtherCharacters(c);
			if (section == c.getBoardSection())
				weight = float.MinValue;
			if (weight > maxWeight) {
				maxWeight = weight;
				maxIndex = i;
			}
		}

		if (maxIndex == -1)
			return;

		BoardSection move = (BoardSection)possibleMoves [maxIndex];
		if (move.getCharacter() != null && move.getCharacter() != c && move.getCharacter().characterType != Character.CharacterType.EnemyNonPlayerCharacter) {
			NPoint p = board.MoveWithinAttackRangeOfMe (move);
			board.MoveToMe (board.boardSections [p.r, p.c]);
			board.DropBoardState ();
			board.SetActiveStateForSection (board.boardSections [p.r, p.c], false);
			board.AttackMe (move);
			board.DropBoardState ();
		} else if (move.getCharacter() == null){
			board.MoveToMe (move);
			board.DropBoardState ();
		}  else {
			possibleMoves.Remove (move);
			DoCPUMoveForCharacter (c, possibleMoves);
		}
	}

	public void ScaleToPartyWithCoefficient(Party p, float coefficient) {
		float otherPSum = 0.0f;
		for (int i = 0; i < p.members.Count; i++) {
			Character c = (Character)p.members [i];
			otherPSum += c.totalHp + c.defense + c.attack + c.agility;
		}

		float pSum = 0.0f;
		for (int i = 0; i < members.Count; i++) {
			Character c = (Character)members [i];
			pSum += c.totalHp + c.defense + c.attack + c.agility;
		}

		float x = otherPSum / pSum;
		x *= coefficient;

		for (int i = 0; i < members.Count; i++) {
			Character c = (Character)members [i];
			c.totalHp *= x / 15;
			c.defense *= x;
			c.attack *= x;
			c.agility *= x;
		}
	}


	public void SetBoard(Board b) {
		board = b;
	}

	public void SavePartyString() {
		String res = "";

		res += "<party>\n";
		foreach (Character c in members) {
			res += c.GetCharacterString ();
		}

		res += "</party>";

		PlayerPrefs.SetString (kPartyStringKey, res);
	}

	public static Party CreatePlayerPartyFromSave() {
		String str = PlayerPrefs.GetString (kPartyStringKey);
		if (str == null || str == "") {
			return null;
		}
		GameObject go = Instantiate(Resources.Load ("Party")) as GameObject;
		Party party = go.GetComponent<Party> () as Party;

		String partyString = str.Substring (str.IndexOf ("<party>") + "<party>\n".Length, str.IndexOf ("</party>") - "</party>".Length);
		String[] partyArr = partyString.Split ('\n');

		while (partyString.Contains ("END")) {
			String currentCharacter = partyString.Substring (0, partyString.IndexOf ("\nEND"));
			partyString = partyString.Substring (partyString.IndexOf ("END") + "END\n".Length);

			Character c = Character.CreateCharacterFromString(currentCharacter);
			c.gameObject.transform.parent = party.gameObject.transform;
			party.members.Add (c);
		}


		party.type = PartyType.Player;
		return party;
	}

}
