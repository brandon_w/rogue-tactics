﻿using UnityEngine;
using System.Collections;

public class PartyFactory : MonoBehaviour {

	public string[] fighterNames = new string[]{ "Fighter 01", "Fighter 02" };
	public string[] ghostNames = new string[]{ "Ghost 01" };
	public string[] mageNames = new string[]{ "Mage 01" };
	public string[] rangerNames = new string[]{ "Ranger 01" };

	public string[][] possibleEnemyParties = new string[][]{
		new string[]{ "Fighter", "Fighter", "Fighter", "Ranger", "Ranger" },
		new string[]{ "Fighter", "Fighter", "Mage", "Fighter", "Mage", "Ranger" },
		new string[]{
			"Ghost",
			"Ghost",
			"Ghost",
			"Ghost",
			"Ghost",
		}
	};

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public Party GenerateAIParty() {
		GameObject p = Instantiate(Resources.Load ("Party")) as GameObject;
		Party party = p.GetComponent<Party> ();
		party.isCPU = true;
		party.SetBoard(GetComponent<Board> ());
		party.type = Party.PartyType.Enemy;

		System.Random rand = new System.Random ();

		int upperbound = possibleEnemyParties.GetUpperBound (0);
		int partyIndex = Random.Range (0,upperbound + 1);
		string[] partyArr = possibleEnemyParties [partyIndex];
		for (int i = 0; i <= partyArr.GetUpperBound (0); i++) {
			string currentCharacter = partyArr [i];

			GameObject go = getCharacterByName (currentCharacter, Character.CharacterType.EnemyNonPlayerCharacter);

			party.members.Add (go.GetComponent<Character> ());
			go.GetComponent<Character> ().party = party;
			go.transform.parent = party.gameObject.transform;

		}
		return party;
	}

	public Party generatePlayerParty() {
		int nFighters = PlayerPrefs.GetInt ("Fighters");
		int nMages = PlayerPrefs.GetInt ("Mages");
		int nRangers = PlayerPrefs.GetInt ("Rangers");

		if (nFighters + nMages + nRangers == 0) {
			nFighters = 3;
			nMages = 1;
			nRangers = 2;
		}

		GameObject p = Instantiate(Resources.Load ("Party")) as GameObject;
		Party party = p.GetComponent<Party> ();
		party.isCPU = false;
		party.SetBoard(GetComponent<Board> ());
		party.type = Party.PartyType.Player;

		System.Random rand = new System.Random ();

		for (int i = 0; i <nFighters; i++) {
			GameObject go = getCharacterByName ("Fighter", Character.CharacterType.PlayerCharacter);

			party.members.Add (go.GetComponent<Character> ());
			go.GetComponent<Character> ().party = party;
			go.transform.parent = party.gameObject.transform;

		}

		for (int i = 0; i <nMages; i++) {
			GameObject go = getCharacterByName ("Mage", Character.CharacterType.PlayerCharacter);

			party.members.Add (go.GetComponent<Character> ());
			go.GetComponent<Character> ().party = party;
			go.transform.parent = party.gameObject.transform;

		}

		for (int i = 0; i <nRangers; i++) {
			GameObject go = getCharacterByName ("Ranger", Character.CharacterType.PlayerCharacter);

			party.members.Add (go.GetComponent<Character> ());
			go.GetComponent<Character> ().party = party;
			go.transform.parent = party.gameObject.transform;

		}

		return party;
	}

	public GameObject getCharacterByName(string currentCharacter, Character.CharacterType type) {
		print (currentCharacter);

		GameObject go;
		string path = "dev/null";
		if (currentCharacter.Equals ("Fighter")) {
			int characterIndex = Random.Range (0, fighterNames.GetUpperBound (0) + 1);
			string characterName = fighterNames [characterIndex];
			path = "characters/fighters/" + characterName;
		} else if (currentCharacter.Equals ("Ranger")) {
			int characterIndex = Random.Range (0, rangerNames.GetUpperBound (0) + 1);
			string characterName = rangerNames [characterIndex];
			path = "characters/rangers/" + characterName;
		} else if (currentCharacter.Equals ("Mage")) {
			int characterIndex = Random.Range (0, mageNames.GetUpperBound (0) + 1);
			string characterName = mageNames [characterIndex];
			path = "characters/mages/" + characterName;
		} else if (currentCharacter.Equals ("Ghost")) {
			int characterIndex = Random.Range (0, ghostNames.GetUpperBound (0) + 1);
			string characterName = ghostNames [characterIndex];
			path = "characters/monsters/" + characterName;
		}

		go = Instantiate (Resources.Load (path)) as GameObject;
		go.GetComponent<Character> ().characterType = type;

		return go;
	}
}
