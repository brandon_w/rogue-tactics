﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public static class FooBaz  {
	public static List<NPoint> GetManhattanRangeForBoard(int[,] arr, int r, int c, int range) {
		List<NPoint> pts = new List<NPoint> ();
		GetManhattanRangeForBoard (arr, r, c, range, pts);
		TrimListAccordingToBoard (pts, arr);
		return pts;
	}

	private  static void GetManhattanRangeForBoard(int[,] arr, int r, int c, int range, List<NPoint> visitedNodes) {
		if (range < 0 || !FooBaz.areIndeciesInRange(arr, r, c) ||  arr[r, c] == 0) {
			return;
		}
		NPoint p = new NPoint (r, c);
		if (!visitedNodes.Contains(p))
			visitedNodes.Add (new NPoint(r, c));
		GetManhattanRangeForBoard (arr, r - 1, c, range - 1, visitedNodes);
		GetManhattanRangeForBoard (arr, r + 1, c, range - 1, visitedNodes);
		GetManhattanRangeForBoard (arr, r, c - 1, range - 1, visitedNodes);
		GetManhattanRangeForBoard (arr, r, c + 1, range - 1, visitedNodes);
	}

	public static void TrimListAccordingToBoard(List<NPoint> vectors, int[,] board) {
		for (int i = vectors.Count - 1; i >= 0; i--) {
			NPoint p = vectors [i];
			int r = vectors [i].r;
			int c = vectors [i].c;

			bool b1 = r < 0 || c < 0;
			bool b2 = r > board.GetUpperBound (0) || c > board.GetUpperBound(1);
			if (b1 || b2) {
				vectors.RemoveAt (i);
			} else if (board [r, c] == 0) {
				vectors.RemoveAt (i);
			}
		}
	}

	public static string getBoardString(int[,] arr)
	{
		string returnString = "";
		for (int i = 0; i <= arr.GetUpperBound (0); i++) {
			for (int j = 0; j <= arr.GetUpperBound (1); j++) {
				if (j == 0) {
					returnString += arr [i, j];
				} else {
					returnString += "," + arr [i, j];
				}
			}

			returnString += "\n";
		}
		return returnString;
	}

	public static bool areIndeciesInRange(int[,] arr, int r, int c) {
		bool b1 = r < 0 || c < 0;
		bool b2 = r > arr.GetUpperBound (0) || c > arr.GetUpperBound(1);
		return !b1 && !b2;
	}

	public static bool removeMeFromList(Character character, List<NPoint> pts) {
		return pts.Remove (new NPoint (character.getBoardSection ().row, character.getBoardSection ().col));
	}

	public static bool isSameTeam(Character c1, Character c2) {
		return 
			(c1.party.type == Party.PartyType.Enemy && c2.party.type == Party.PartyType.Enemy) ||
			(c1.party.type == Party.PartyType.Player && c2.party.type == Party.PartyType.Player) ||
			(c1.party.type == Party.PartyType.Neutral && c2.party.type == Party.PartyType.Neutral) || c1 == c2;
	}

	private class Pt {
		public NPoint point;
		public int dist;
		public Pt parent;
	}

	public static List<NPoint> findPath(int[,] board, List<NPoint> moveLocations, NPoint start, NPoint dest) {

		Pt[,] arr = new Pt[board.GetUpperBound (0) + 1, board.GetUpperBound (1) + 1];

		for (int i = 0; i <= arr.GetUpperBound (0); i++) {
			for (int j = 0; j < arr.GetUpperBound (1); j++) {
				arr [i, j] = null;
			}
		}

		foreach (NPoint pt in moveLocations) {
			Pt p = new Pt ();
			p.point = pt;
			p.dist = int.MaxValue;
			p.parent = null;

			arr [pt.r, pt.c] = p;
		}

		Queue<Pt> pts = new Queue<Pt> ();

		Pt root = new Pt ();
		root.point = start;
		root.dist = int.MaxValue;
		root.parent = null;

		pts.Enqueue (root);

		Pt goal = null;

		while (pts.Count != 0) {
			Pt p = pts.Dequeue ();

			if (p.point.Equals (dest)) {
				goal = p;
				break;
			}

			int r = p.point.r;
			int c = p.point.c;

			if (isInBounds (board, r - 1, c)) {
				Pt north = arr [r - 1, c];
				if (north != null && north.dist == int.MaxValue) {
					

					north.dist = p.dist + 1;
					north.parent = p;
					pts.Enqueue (north);
				}
			}

			if (isInBounds (board, r - 1, c + 1)) {
				Pt northeast = arr [r - 1, c + 1];
				if (northeast != null && northeast.dist == int.MaxValue) {
					northeast.dist = p.dist + 1;
					northeast.parent = p;
					pts.Enqueue (northeast);
				}
			}

			if (isInBounds (board, r, c + 1)) {
				Pt east = arr [r, c + 1];
				if (east != null && east.dist == int.MaxValue) {
					east.dist = p.dist + 1;
					east.parent = p;
					pts.Enqueue (east);
				}
			}

			if (isInBounds (board, r + 1, c + 1)) {
				Pt southeast = arr [r + 1, c + 1];
				if (southeast != null && southeast.dist == int.MaxValue) {
					southeast.dist = p.dist + 1;
					southeast.parent = p;
					pts.Enqueue (southeast);
				}
			}

			if (isInBounds (board, r + 1, c)) {
				Pt south = arr [r + 1, c];
				if (south != null && south.dist == int.MaxValue) {
					south.dist = p.dist + 1;
					south.parent = p;
					pts.Enqueue (south);
				}
			}

			if (isInBounds (board, r + 1, c - 1)) {
				Pt southwest = arr [r + 1, c - 1];
				if (southwest != null && southwest.dist == int.MaxValue) {
					southwest.dist = p.dist + 1;
					southwest.parent = p;
					pts.Enqueue (southwest);
				}
			}

			if (isInBounds (board, r, c - 1)) {
				Pt west = arr [r, c - 1];
				if (west != null && west.dist == int.MaxValue) {
					west.dist = p.dist + 1;
					west.parent = p;
					pts.Enqueue (west);
				}
			}

			if (isInBounds (board, r - 1, c - 1)) {
				Pt northwest = arr [r - 1, c - 1];
				if (northwest != null && northwest.dist == int.MaxValue) {
					northwest.dist = p.dist + 1;
					northwest.parent = p;
					pts.Enqueue (northwest);
				}
			}
		}

		if (goal != null) {
			return unpackGoalNode (goal);
		}
		return null;
	}

	private static List<NPoint> unpackGoalNode(Pt goal) {
		List<NPoint> pts = new List<NPoint> ();

		while (goal != null) {
			pts.Add (goal.point);
			goal = goal.parent;
		}

		pts.Reverse ();
		return pts;
	}



	public static bool VectorEquals(Vector3 a, Vector3 b, float thresh) {
		if ((a - b).sqrMagnitude <= (a * thresh).sqrMagnitude) {
			return true;
		}
		return false;
	}

	public static bool isInBounds(int[,] bounds, int r, int c) {
		return !(r < 0 || c < 0 || r > bounds.GetUpperBound (0) || c > bounds.GetUpperBound (1));
	}
	/*
	public static List<NPoint> findPath(int[,] board, List<NPoint> moveLocations, NPoint start, NPoint dest) {
		int[,] arr = new int[board.GetUpperBound (0) + 1, board.GetUpperBound (1) + 1];

		for (int i = 0; i <= arr.GetUpperBound (0); i++) {
			for (int j = 0; j < arr.GetUpperBound (1); j++) {
				arr [i, j] = 0;
			}
		}

		foreach (NPoint p in moveLocations) {
			arr [p.r, p.c] = 1;
		}

		List<NPoint> pts = new List<NPoint> ();
		if (findPath_ (arr, start, dest, pts))
			return pts;
		return null;
	}

	private static bool findPath_(int[,] board, NPoint current, NPoint dest, List<NPoint> pts) {
		if (current.r < 0 || current.c < 0 || current.r > board.GetUpperBound (0) || current.c > board.GetUpperBound (1))
			return false;
		if (board [current.r, current.c] != 1 || pts.Contains(current))
			return false;

		pts.Add (current);

		if (current.Equals (dest))
			return true;

		if (findPath_ (board, new NPoint (current.r - 1, current.c), dest, pts))
			return true;
		if (findPath_ (board, new NPoint (current.r, current.c + 1), dest, pts))
			return true;
		if (findPath_ (board, new NPoint (current.r, current.c - 1), dest, pts))
			return true;
		if (findPath_ (board, new NPoint (current.r + 1, current.c), dest, pts))
			return true;
		pts.Remove (dest);
		return false;
	}
*/

}
