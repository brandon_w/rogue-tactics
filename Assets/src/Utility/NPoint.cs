﻿using UnityEngine;
using System.Collections;

public class NPoint{
	public int r, c;
	public NPoint(int r, int c) {
		this.r = r;
		this.c = c;
	}

	public override bool Equals (object obj)
	{
		if (obj == null)
			return false;
		NPoint other = obj as NPoint;
		if (other as object == null)
			return false;
		return other.r == this.r && other.c == this.c;
	}

	public override string ToString ()
	{
		return string.Format ("[NPoint] = {0}, {1}", r, c);
	}
}
