﻿using UnityEngine;
using System.Collections.Generic;

public class PlayerPrefsUtil : MonoBehaviour {
	/*
	 * Constants
	 */
	public static string kCharacterDeadList = "Dead";
	public static string kCurrentParty = "Party";
	public static string kVictoryCount = "Victories";
	public static string kDefeatCount = "defeats";
		
	/*
	 * Writes the given character guid to the dead list
	 */
	public static void writeFriendlyCharacterToDeadList(Character character)
	{
		string deadList = PlayerPrefs.GetString (kCharacterDeadList);

		if (deadList == null) {
			deadList = "" + character.guid;
		} else if (deadList.Length < 1) {
			deadList = "" + character.guid;
		} else {
			deadList += "," + character.guid;
		}

		PlayerPrefs.SetString (kCharacterDeadList, deadList);
	}

	/*
	 * Writes the character out to it's own key/val key=guid, val=characterstring
	 */
	public static void writeCharacterToGuid(Character character) 
	{
		string characterString = character.GetCharacterString ();

		if (characterString == null) {
			return;
		} else if (characterString.Length < 1) {
			return;
		}

		// note, this will overwrite any character previously stored here.
		// highly unlikely
		// much more likely is that this is replacing the same character with an update batched after each battle
		PlayerPrefs.SetString(character.guid, characterString);
	}

	/*
	 * Reads & instantiates player party from party string
	 */
	public static List<Character> readCharactersForParty()
	{
		List<Character> partyMembers = new List<Character> ();

		string characterListString = PlayerPrefs.GetString (kCurrentParty);
		if (characterListString == null) {
			return null;
		} else if (characterListString.Length < 1) {
			return null;
		}

		string[] characterList = characterListString.Split(new char[]{','});
		if (characterList.Length < 1) {
			return null;
		}

		foreach (string characterString in characterList) {
			Character character = Character.CreateCharacterFromString(characterString);
			partyMembers.Add (character);
		}

		return partyMembers;
	}

	/*
	 * Increment victory count
	 */
	public static void incrementVictoryCount()
	{
		int victoryCount = PlayerPrefs.GetInt (kVictoryCount);
		victoryCount += 1;
		PlayerPrefs.SetInt (kVictoryCount, victoryCount);
	}

	/*
	 * Increment defeat count
	 */ 
	public static void incrementDefeatCount()
	{
		int defeatCount = PlayerPrefs.GetInt (kDefeatCount);
		defeatCount += 1;
		PlayerPrefs.SetInt (kDefeatCount, defeatCount);
	}
}
