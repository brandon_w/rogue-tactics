﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;

public class Character : RogueGameObject {

	public const int RANGER_MOVE_RANGE = 6;
	public const int FIGHTER_MOVE_RANGE = 6;
	public const int MAGE_MOVE_RANGE = 5;

	public const int RANGER_ATTACK_RANGE = 3;
	public const int FIGHTER_ATTACK_RANGE = 1;
	public const int MAGE_ATTACK_RANGE = 2;

	public enum CharacterType
	{
		PlayerCharacter,
		EnemyNonPlayerCharacter,
		NuetralNonPlayerCharacter
	}

	public enum CharacterClass
	{
		Fighter,
		Mage,
		Ranger
	}

	public CharacterClass characterClass;

	public class SkillTreeNode
	{
		public float armorBonus, attackBonus, agilityBonus, defenseBonus, hpBonus, apBonus;
		public Ability newAbility;
		public List<SkillTreeNode> children = new List<SkillTreeNode>();
	}

	//the root of what we use at runtime. not serialized.
	SkillTreeNode root = new SkillTreeNode(); 

	public TextMesh healthText;

	public CharacterType characterType;

	public String path;
	public String guid;

	// how many action points you have this turn
	public float ap;
	public float totalAp;

	// how many hit points the player has total
	public float hp;
	public float totalHp;

	// how much damage you'll hit for
	public float attack;

	// probability you'll hit
	public float agility;

	// damage reduction
	public float defense;

	public Ability activeAbility;

	private SpriteRenderer spriteRenderer;

	private BoardSection section;

	public GameObject icon;

	public List<AbilityEffect> abilityEffects;

	public Party party;

	public bool immuneToDamage;
	public int currentMoveRange;

	// Use this for initialization
	public void Awake () {
		spriteRenderer = GetComponent<SpriteRenderer> ();
		hp = totalHp;
		ap = totalAp;

		GameObject go;
		switch (characterClass) {
		case CharacterClass.Fighter:
			go = Instantiate (Resources.Load ("abilities/Immunity Ability")) as GameObject;
			activeAbility = go.GetComponent<Ability> ();
			go.transform.SetParent (gameObject.transform);
			break;
		case CharacterClass.Ranger:
			go = Instantiate (Resources.Load ("abilities/Stun Ability")) as GameObject;
			activeAbility = go.GetComponent<Ability> ();
			go.transform.SetParent (gameObject.transform);
			break;
		default:
			go = Instantiate (Resources.Load ("abilities/Fireblast Ability")) as GameObject;
			activeAbility = go.GetComponent<Ability> ();
			go.transform.SetParent (gameObject.transform);
			break;
		}
			

		currentMoveRange = getMoveRange ();
		immuneToDamage = false;
	}
	
	// Update is called once per frame
	void LateUpdate () {
		healthText.text = "" + (int)hp;

		if (ap <= 0.0f) {
			spriteRenderer.color = new Color (spriteRenderer.color.r, spriteRenderer.color.g, spriteRenderer.color.b, .5f);
		} else {
			spriteRenderer.color = new Color (spriteRenderer.color.r, spriteRenderer.color.g, spriteRenderer.color.b, 1f);
		}
	}

	public void setBoardSection(BoardSection section) {
		this.section = section;
	}

	public BoardSection getBoardSection() {
		return this.section;
	}

	public void addEffect(AbilityEffect ae) {
		if (ae.effectType == AbilityEffect.EffectType.ImmuneToDamage)
			immuneToDamage = true;

		ae.character = this;
		ae.gameObject.transform.SetParent (ae.transform);
		abilityEffects.Add (ae);
	}

	public void doTurnStart() {
		for(int i = abilityEffects.Count - 1; i >= 0; i--) {
			AbilityEffect e = abilityEffects[i].ApplyEffect ();
			if (e != null) {
				abilityEffects.Remove (e);
			}
		}
	}

	public void doTurnEnd() {
		immuneToDamage = false;
		currentMoveRange = getMoveRange ();
	}

	public int getMoveRange() {
		switch (characterClass) {
		case CharacterClass.Fighter:
			return FIGHTER_MOVE_RANGE;
		case CharacterClass.Mage:
			return MAGE_MOVE_RANGE;
		case CharacterClass.Ranger:
			return RANGER_MOVE_RANGE;
		default:
			return 0;
		}
	}

	public bool applyDamage(float dmg) {
		if (immuneToDamage)
			return false;

		hp -= dmg;
		if (hp <= 0) {
			return true;
		} else {
			return false;
		}
	}

	public bool applyAbility(Ability ability) {
		addEffect (ability.abilityEffect);
		return applyDamage (ability.damage);
	}

	////////////////////////////////////////////
	/// //////////////////////////////////////
	/// ////////////////////////////////////
	/// //////////////////////////////////
	/// Attack ranges

	public List<NPoint> GetAttackSections(int[,] arr, int start_row, int start_col) {
		switch (characterClass) {
		case CharacterClass.Fighter:
			print ("DEBUG: GetAttackSections: Fighter");
			return FighterAttackRange (arr, start_row, start_col);
		case CharacterClass.Mage:
			print ("DEBUG: GetAttackSections: Mage");
			return MageAttackRange (arr, start_row, start_col);
		case CharacterClass.Ranger:
			print ("DEBUG: GetAttackSections: Ranger");
			return RangerAttackRange (arr, start_row, start_col);
		default:
			print ("DEBUG: GetAttackSections: Default");
			return FighterAttackRange (arr, start_row, start_col);
		}
	}

	private List<NPoint> FighterAttackRange(int[,] arr, int start_row, int start_col) {
		List<NPoint> vectors;
		vectors = FooBaz.GetManhattanRangeForBoard (getBoardSection().board.collision, start_row, start_col, FIGHTER_ATTACK_RANGE);
		FooBaz.TrimListAccordingToBoard (vectors, arr);
		FooBaz.removeMeFromList (this, vectors);
		return vectors;
	}

	private List<NPoint> RangerAttackRange(int[,] arr, int start_row, int start_col) {
		List<NPoint> allPts;
		allPts = FooBaz.GetManhattanRangeForBoard (getBoardSection().board.collision, start_row, start_col, RANGER_ATTACK_RANGE);
		List<NPoint> noAtkPts;
		noAtkPts = FooBaz.GetManhattanRangeForBoard (getBoardSection().board.collision, start_row, start_col, RANGER_ATTACK_RANGE - 1);

		List<NPoint> vectors = allPts.Except(noAtkPts).ToList();
		FooBaz.TrimListAccordingToBoard (vectors, arr);
		FooBaz.removeMeFromList (this, vectors);
		return vectors;
	}

	private List<NPoint> MageAttackRange(int[,] arr, int start_row, int start_col) {
		List<NPoint> allPts;
		allPts = FooBaz.GetManhattanRangeForBoard (getBoardSection().board.collision, start_row, start_col, MAGE_ATTACK_RANGE);
		List<NPoint> noAtkPts;
		noAtkPts = FooBaz.GetManhattanRangeForBoard (getBoardSection().board.collision, start_row, start_col, MAGE_ATTACK_RANGE - 1);

		List<NPoint> vectors = allPts.Except(noAtkPts).ToList();
		FooBaz.TrimListAccordingToBoard (vectors, arr);
		FooBaz.removeMeFromList (this, vectors);
		return vectors;
	}


	////////////////////////////////////////////
	/// //////////////////////////////////////
	/// ////////////////////////////////////
	/// //////////////////////////////////
	/// Move Ranges
	public List<NPoint> GetMoveSections(int[,] arr, int start_row, int start_col) {
		switch (characterClass) {
		case CharacterClass.Fighter:
			print ("DEBUG: GetMoveSections: Fighter");
			return FighterMoveRange (arr, start_row, start_col);
		case CharacterClass.Mage:
			print ("DEBUG: GetMoveSections: Mage");
			return MageMoveRange (arr, start_row, start_col);
		case CharacterClass.Ranger:
			print ("DEBUG: GetMoveSections: Ranger");
			return RangerMoveRange(arr, start_row, start_col);
		default:
			print ("DEBUG: GetMoveSections: Default");
			return FighterMoveRange (arr, start_row, start_col);
		}
	}

	private List<NPoint> FighterMoveRange(int[,] arr, int start_row, int start_col) {
		List<NPoint> vectors;
		vectors = FooBaz.GetManhattanRangeForBoard (getBoardSection().board.collision, start_row, start_col, currentMoveRange - 3);

		int rup, rdown, cleft, cright;
		for (int i = 0; i < currentMoveRange; i++) {
			rup = start_row - i;

			if (!FooBaz.areIndeciesInRange(arr, rup, start_col) || arr [rup, start_col] == 0)
				break;

			vectors.Add (new NPoint (rup, start_col));
		}

		for (int i = 0; i < currentMoveRange; i++) {

			rdown = start_row + i;

			if (!FooBaz.areIndeciesInRange(arr, rdown, start_col) || arr [rdown, start_col] == 0)
				break;

			vectors.Add (new NPoint (rdown, start_col));
		}

		for (int i = 0; i < currentMoveRange; i++) {

			cleft = start_col - i;

			if (!FooBaz.areIndeciesInRange(arr, start_row, cleft) || arr [start_row, cleft] == 0)
				break;

			vectors.Add (new NPoint (start_row, cleft));
		}

		for (int i = 0; i < currentMoveRange; i++) {

			cright = start_col + i;

			if (!FooBaz.areIndeciesInRange(arr, start_row, cright) || arr [start_row, cright] == 0)
				break;

			vectors.Add (new NPoint (start_row, cright));
		}
		vectors = vectors.Distinct ().ToList ();
		FooBaz.TrimListAccordingToBoard (vectors, arr);
		FooBaz.removeMeFromList (this, vectors);

		return vectors;
	}

	private List<NPoint> RangerMoveRange(int[,] arr, int start_row, int start_col) {
		List<NPoint> vectors;
		vectors = FooBaz.GetManhattanRangeForBoard(getBoardSection().board.collision, start_row, start_col, currentMoveRange/2);

		// 
		// Upper-left, plus add middle
		int cur_row = start_row - 1, cur_col = start_col - 1;
		int cur_range = 0;
		while (cur_range++ < currentMoveRange &&
				cur_row >= 0 && cur_row <= arr.GetUpperBound (0) && 
			 	cur_col >= 0 && cur_col <= arr.GetUpperBound (1) && arr [cur_row, cur_col] != 0) {
			vectors.Add (new NPoint (cur_row, cur_col));
			cur_row -= 1;
			cur_col -= 1;
		}

		cur_row = start_row + 1;
		cur_col = start_col - 1;
		cur_range = 0;
		while (cur_range++ < currentMoveRange &&
				cur_row >= 0 && cur_row <= arr.GetUpperBound (0) && 
				cur_col >= 0 && cur_col <= arr.GetUpperBound (1) && arr [cur_row, cur_col] != 0) {
			vectors.Add (new NPoint (cur_row, cur_col));
			cur_row += 1;
			cur_col -= 1;
		}

		cur_row = start_row + 1;
		cur_col = start_col + 1;
		cur_range = 0;
		while (cur_range++ < currentMoveRange &&
				cur_row >= 0 && cur_row <= arr.GetUpperBound (0) && 
				cur_col >= 0 && cur_col <= arr.GetUpperBound (1) && arr [cur_row, cur_col] != 0) {
			vectors.Add (new NPoint (cur_row, cur_col));
			cur_row += 1;
			cur_col += 1;
		}

		cur_row = start_row - 1;
		cur_col = start_col + 1;
		cur_range = 0;
		while (cur_range++ < currentMoveRange &&
				cur_row >= 0 && cur_row <= arr.GetUpperBound (0) && 
				cur_col >= 0 && cur_col <= arr.GetUpperBound (1) && arr [cur_row, cur_col] != 0) {
			vectors.Add (new NPoint (cur_row, cur_col));
			cur_row -= 1;
			cur_col += 1;
		}

		FooBaz.removeMeFromList (this, vectors);
		FooBaz.TrimListAccordingToBoard (vectors, getBoardSection ().board.collision);
		return vectors;
	}

	private List<NPoint> MageMoveRange(int[,] arr, int start_row, int start_col) {
		List<NPoint> vectors;
		vectors = FooBaz.GetManhattanRangeForBoard (getBoardSection().board.collision, start_row, start_col, currentMoveRange - 3);


		//
		// Top, mid to top
		//
		int rup, rdown, cleft, cright;
		bool cancelWings = false;
		for (int i = 0; i < currentMoveRange; i++) {
			rup = start_row - i;

			if (! FooBaz.areIndeciesInRange(arr, rup, start_col) || arr [rup, start_col] == 0) {
				cancelWings = true;
				break;
			}

			vectors.Add (new NPoint (rup, start_col));
		}

		//
		// Top, top mid to right
		//
		for (int i = 0; i < currentMoveRange + 1; i++) {
			if (cancelWings) {
				break;
			}

			cright = start_col + i;

			if (!FooBaz.areIndeciesInRange(arr, start_row - currentMoveRange, cright) || arr [start_row - currentMoveRange, cright] == 0) {
				break;
			}

			vectors.Add (new NPoint (start_row - currentMoveRange, cright));
		}

		//
		// Top, top mid to left
		//
		for (int i = 0; i < currentMoveRange + 1; i++) {
			if (cancelWings) {
				break;
			}

			cleft = start_col - i;

			if (!FooBaz.areIndeciesInRange(arr, start_row - currentMoveRange, cleft) || arr [start_row - currentMoveRange, cleft] == 0) {
				break;
			}

			vectors.Add (new NPoint (start_row - currentMoveRange, cleft));
		}

		//
		// bot, mid to bot
		//
		cancelWings = false;
		for (int i = 0; i < currentMoveRange; i++) {
			rdown = start_row + i;

			if (!FooBaz.areIndeciesInRange(arr, rdown, start_col) || arr [rdown, start_col] == 0) {
				cancelWings = true;
				break;
			}

			vectors.Add (new NPoint (rdown, start_col));
		}

		//
		// bot mid to right
		//
		for (int i = 0; i < currentMoveRange + 1; i++) {
			if (cancelWings) {
				break;
			}

			cright = start_col + i;

			if (!FooBaz.areIndeciesInRange(arr, start_row + currentMoveRange, cright) || arr [start_row + currentMoveRange, cright] == 0) {
				break;
			}

			vectors.Add (new NPoint (start_row + currentMoveRange, cright));
		}

		//
		// bot mid to left
		//
		for (int i = 0; i < currentMoveRange + 1; i++) {
			if (cancelWings) {
				break;
			}

			cleft = start_col - i;

			if (!FooBaz.areIndeciesInRange(arr, start_row + currentMoveRange, cleft) || arr [start_row + currentMoveRange, cleft] == 0) {
				break;
			}

			vectors.Add (new NPoint (start_row + currentMoveRange, cleft));

		}

		//
		// left, mid to left
		//
		cancelWings = false;
		for (int i = 0; i < currentMoveRange; i++) {
			cleft = start_col - i;

			if (!FooBaz.areIndeciesInRange(arr, start_row, cleft) || arr [start_row, cleft] == 0) {
				cancelWings = true;
				break;
			}

			vectors.Add (new NPoint (start_row, cleft));
		}

		//
		// left, left mid to bottom
		//
		for (int i = 0; i < currentMoveRange + 1; i++) {
			if (cancelWings) {
				break;
			}

			rdown = start_row + i;

			if (!FooBaz.areIndeciesInRange(arr, rdown, start_col - currentMoveRange) || arr [rdown, start_col - currentMoveRange] == 0) {
				break;
			}
				
			vectors.Add (new NPoint (rdown, start_col - currentMoveRange));
		}

		//
		// left, left mid to top
		//
		for (int i = 0; i < currentMoveRange + 1; i++) {
			if (cancelWings) {
				break;
			}

			rup = start_row - i;

			if (!FooBaz.areIndeciesInRange(arr, rup, start_col - currentMoveRange) || arr [rup, start_col - currentMoveRange] == 0) {
				break;
			}

			vectors.Add (new NPoint (rup, start_col - currentMoveRange));
		}

		//
		// right, mid to right
		//
		cancelWings = false;
		for (int i = 0; i < currentMoveRange; i++) {
			cright = start_col + i;

			if (!FooBaz.areIndeciesInRange(arr, start_row, cright) || arr [start_row, cright] == 0) {
				cancelWings = true;
				break;
			}

			vectors.Add (new NPoint (start_row, cright));
		}

		//
		// right, right mid to top
		//
		for (int i = 0; i < currentMoveRange + 1; i++) {
			if (cancelWings) {
				break;
			}

			rup = start_row - i;

			if (!FooBaz.areIndeciesInRange(arr, rup, start_col + currentMoveRange) || arr [rup, start_col + currentMoveRange] == 0) {
				break;
			}

			vectors.Add (new NPoint (rup, start_col + currentMoveRange));
		}

		//
		// right, right mid to bot
		//
		for (int i = 0; i < currentMoveRange + 1; i++) {
			if (cancelWings) {
				break;
			}

			rdown = start_row + i;

			if (!FooBaz.areIndeciesInRange(arr, rdown, start_col + currentMoveRange) || arr [rdown, start_col + currentMoveRange] == 0) {
				break;
			}

			vectors.Add (new NPoint (rdown, start_col + currentMoveRange));
		}

		/*vectors.Add (new NPoint (start_row - (currentMoveRange), start_col - (currentMoveRange)));
		vectors.Add (new NPoint (start_row + (currentMoveRange), start_col - (currentMoveRange)));
		vectors.Add (new NPoint (start_row - (currentMoveRange), start_col + (currentMoveRange)));
		vectors.Add (new NPoint (start_row + (currentMoveRange), start_col + (currentMoveRange)));*/

		vectors = vectors.Distinct ().ToList ();
		FooBaz.TrimListAccordingToBoard (vectors, arr);
		FooBaz.removeMeFromList (this, vectors);

		return vectors;
	}

	public bool CanAttackAtDistance(int d) {
		switch (characterClass) {
		case CharacterClass.Fighter:
			return d <= FIGHTER_ATTACK_RANGE;
		case CharacterClass.Mage:
			return d <= MAGE_ATTACK_RANGE;
		case CharacterClass.Ranger:
			return d <= RANGER_ATTACK_RANGE;
		}

		return false;
	}

	////////////////////////////////////////////
	/// //////////////////////////////////////
	/// ////////////////////////////////////
	/// //////////////////////////////////
	/// Serialization stuff down here

	/* Path: 
	 * GUID: 
	 * TotalHealth:
	 * TotalActionPoints:
	 * Attack:
	 * Agility:
	 * Defense:
	 * CharacterClass:
	 * END
	 */

	public String GetCharacterString() { 
		String cclass = "";
		switch (characterClass) {
		case CharacterClass.Mage:
			cclass = "Mage";
			break;
		case CharacterClass.Fighter:
			cclass = "Fighter";
			break;
		case CharacterClass.Ranger:
			cclass = "Ranger";
			break;
		default:
			cclass = "Unknown";
			break;
		}
		String result = "Path: " + path + "\nGUID: " + guid + "\nTotalHealth: " + totalHp + "\nTotalActionPoints: " + totalAp + "\nAttack: " + attack + "\nAgility: " + agility + "\nDefense: " + defense + "\nCharacterClass: " + cclass + "\nEND\n";
		return result;
	}

	public static Character CreateCharacterFromString(String str) {
		String[] arr = str.Split ('\n');
		String path, guid, cclass;
		float ap, hp, attack, agility, defense;
		path = "";
		attack = 0;
		defense = 0;
		agility = 0;
		guid = "";
		ap = 0;
		hp = 0;


		foreach (String s in arr) {
			String[] keyVal = s.Split (':');
			String key = keyVal [0];
			String val = keyVal [1];
			key.Trim ();
			val.Trim();

			if (key == "Path") {
				path = val;
			} else if (key == "GUID") {
				guid = val;
			} else if (key == "TotalHealth") {
				hp = float.Parse(val);
			} else if (key == "TotalActionPoints") {
				ap = float.Parse(val);
			} else if (key == "Attack") {
				attack = float.Parse (val);
			} else if (key == "Agility") {
				agility = float.Parse (val);
			} else if (key == "Defense") {
				defense = float.Parse (val);
			} else if (key == "CharacterClass") {
				cclass = val;
			}
		}

		Character c = CreateCharacterFromPath (path);
		c.ap = ap;
		c.hp = hp;
		c.attack = attack;
		c.defense = defense;
		c.agility = agility;
		c.path = path;
		c.guid = guid;
		c.characterType = CharacterType.PlayerCharacter;
		return c;

	}

	public static Character CreateCharacterFromPath(String path) {
		path = path.Trim ();
		//GameObject go = Instantiate(Resources.Load ("characters/fighters/Fighter 01")) as GameObject;
		GameObject go = Instantiate(Resources.Load (path)) as GameObject;
		//GameObject go = Instantiate(Resources.Load ("characters/fighters/Fighter 01")) as GameObject;
		go.GetComponent<Character> ().guid = Guid.NewGuid ().ToString ();
		go.GetComponent<Character> ().path = path;
		Character c = go.GetComponent<Character> ();


		//GameObject icon = Instantiate (Resources.Load (path + " Icon")) as GameObject;
		go.GetComponent<Character>().icon.gameObject.SetActive (false);
		//go.GetComponent<Character> ().icon = icon;

		//go.GetComponent<Character>().icon.transform.parent = go.transform;

		return go.GetComponent<Character> ();
	}
}
