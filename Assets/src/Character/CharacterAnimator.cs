﻿using UnityEngine;
using System.Collections.Generic;

public class CharacterAnimator : MonoBehaviour {

	public float speed;
	public List<Vector3> nodes;
	public Board board;
	private BoardSection target;
	private bool attackIntended;


	// Use this for initialization
	void Awake () {
		if (GameObject.FindGameObjectWithTag ("Board") != null) {
			board = GameObject.FindGameObjectWithTag ("Board").GetComponent<Board> ();
		}
		speed = 1;
		attackIntended = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (nodes != null && nodes.Count > 0) {
			if (FooBaz.VectorEquals(transform.position, nodes[0], .001f)) {

				if (nodes.Count == 1) {
					transform.position = nodes [0];
				}
				nodes.RemoveAt (0);
				if (nodes.Count == 0 && attackIntended) {
					attackIntended = false;
					board.finishMoveToMeForAttack (target);
				}
			} else {
				transform.position = Vector3.MoveTowards (transform.position, nodes [0], Time.deltaTime * speed);
			}
		}
	}

	public void setPts(List<NPoint> pts) 
	{
		nodes = new List<Vector3> ();
		for (int i = 0; i < pts.Count; i++) {
			NPoint pt = pts [i];
			Vector3 v = board.boardSections[pt.r, pt.c].transform.position;
			v = new Vector3(v.x, v.y, -1);
			nodes.Add(v);
		}
	}

	public void setIntendedAttackTarget(BoardSection section) {
		attackIntended = true;
		target = section;
	}
}
