﻿using UnityEngine;
using System.Collections.Generic;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class Polygon : MonoBehaviour {

    private const float DEFAULT_RADIUS = 1.0f;
    private const int DEFAULT_QUALITY = 360;

    public float radius;
    public int quality;

    // Components
    private MeshFilter meshFilter;
    private MeshRenderer meshRenderer;

    // Derived
    private Mesh mesh;

    // My Properties
    private Vector3[] vertices;


    void Awake() {
        meshFilter = GetComponent<MeshFilter>();
        meshRenderer = GetComponent<MeshRenderer>();

        if (radius <= 0) {
            radius = DEFAULT_RADIUS;
        }

        if (quality <= 0) {
            quality = DEFAULT_QUALITY;
        }

        Generate();
    }

    private void Generate() {
        meshFilter.mesh = mesh = new Mesh();
        mesh.name = "Polygon!";

        vertices = new Vector3[quality + 1];
        vertices[0] = Vector3.zero;
        // start off at 1
        int vi = 1;
        float degreesPerTick = 360.0f / quality;
		float angleOffset = 0.0f;
		if (quality == 3) {
			angleOffset = 90.0f;
		} else if (quality == 4) {
			angleOffset = 45.0f;
		}
        for (int i = 1; i <= quality; i++) {
			vertices[vi] = new Vector3(Mathf.Cos(Mathf.Deg2Rad * angleOffset + 
                                                    Mathf.Deg2Rad * degreesPerTick * i), 
                                         Mathf.Sin(Mathf.Deg2Rad * angleOffset + Mathf.Deg2Rad *                                          degreesPerTick * i), 
                                         0);
            vertices[vi] = Vector3.Scale(vertices[vi], transform.lossyScale);
            vi++;
        }
        mesh.vertices = vertices;

        int[] triangles = new int[quality * 3];
        int ti = 0;
        for (int i = 1; i < vertices.Length; i++) {
            triangles[ti++] = 0;


            if (i == vertices.Length - 1) {
                triangles[ti++] = 1;
            } else {
                triangles[ti++] = i + 1;
            }

            triangles[ti++] = i;

        }
        mesh.triangles = triangles;
        // mesh.RecalculateNormals();

        if (gameObject.GetComponent<PolygonCollider2D>() == null)
            gameObject.AddComponent<PolygonCollider2D>();

        PolygonCollider2D polygonCollider2D = gameObject.GetComponent<PolygonCollider2D>();
        polygonCollider2D.points = GetConvexHull();
    }

    void Update() {
        if (quality != vertices.Length - 1) {
            Generate();
        }

    }

    public Vector2[] GetConvexHull() {
        Vector2[] pts = new Vector2[vertices.Length - 1];

        // vertices
        for (int i = 1; i < vertices.Length; i++) {
            pts[i - 1] = vertices[i];
        }
        return pts;
    }

    public Bounds GetBounds() {
        return meshRenderer.bounds;
    }

    public void SetColor(Color color) {
        meshRenderer.material = new Material(Shader.Find("Sprites/Default"));
        meshRenderer.material.color = color;
    }

    public void SetQuality(int quality) {
        this.quality = quality;
        Generate();
    }

    public bool PointNearPolygon(Vector2 point, float range, float step) {
        for (float i = -range; i <= range; i += step) {
           for (float j = -range; j <= range; j += step) {
                Vector2 pt = new Vector2(point.x + i, point.y + j);
                if (PointInsidePolygon(pt)) {
                    return true;
                }
           } 
        }

        return false;;
    }

	public bool PointInsidePolygon(Vector2 point) {
		return GetComponent<PolygonCollider2D> ().OverlapPoint (point);
	}

	public static Polygon CreatePolygon(int quality = 360, Transform parent = null) {
		GameObject ob = new GameObject();
		if (parent != null)
			ob.transform.parent = parent;
		
		Polygon polygon = ob.AddComponent<Polygon>();
		polygon.SetQuality(quality);

		return polygon;
	}
}
