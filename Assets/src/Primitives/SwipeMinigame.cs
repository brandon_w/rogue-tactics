﻿using UnityEngine;
using System.Collections;

public class SwipeMinigame : MonoBehaviour
{
    private const float BGLayer = 0.0f;
    private const float FHLayer = -0.1f;
    private const float DLayer = -0.2f;

    public int nFillPolygons;
    public GameObject polygonHolder;

    public TextMesh textCounter;
    public GameObject checkMark;
    public GameObject crossMark;
    public TextMesh textInfo;

    public float timer;

    Polygon squarePolygon;
    Polygon[] fillPolygons;
    bool[] fillPolygonFlags;

    public bool completed;
    public bool success;

    private bool active = false;

    // Use this for initialization
    void Start()
    {
        checkMark.gameObject.SetActive(false);
        crossMark.gameObject.SetActive(false);
        textCounter.text = string.Format("{0:00.00}", timer);

        Vector3 scale = transform.localScale;
        transform.localScale = new Vector3(1, 1);

        Quaternion rotation = transform.localRotation;
        transform.rotation = Quaternion.Euler(0, 0, 0);

        // Note: This has to be after resetting the local scale and transform
        // to avoid unintentionally altering the transform of this child object.
        polygonHolder = new GameObject();
        polygonHolder.transform.SetParent(this.transform);

        Generate();

        transform.localScale = scale;
        transform.rotation = rotation;
        polygonHolder.transform.rotation = Quaternion.Euler(0, 0, Random.Range(0.0f, 360.0f));

        StartCoroutine(Rotate());
    }

    public IEnumerator Rotate()
    {
        float degrees = Random.Range(0.0f, 360.0f);
        while (degrees > 0.0f)
        {
            degrees -= 5.0f;
            polygonHolder.transform.Rotate(new Vector3(0, 0, 5.0f));
            yield return new WaitForSeconds(.01f);

        }

        active = true;
    }

    private void Generate()
    {
        Polygon outlinePolygon = Polygon.CreatePolygon(360, polygonHolder.transform);
        outlinePolygon.gameObject.name = "Outline";
        outlinePolygon.SetColor(Color.white);
        Vector3 pos = outlinePolygon.transform.localPosition;
        pos.z = BGLayer;

        squarePolygon = Polygon.CreatePolygon(4, outlinePolygon.transform);
        squarePolygon.SetColor(Color.red);
        pos = squarePolygon.transform.localPosition;
        pos.z = FHLayer;
        squarePolygon.transform.localPosition = pos;
        squarePolygon.transform.localScale = new Vector3(1.35f, .1f, 0.0f);

        Bounds b = squarePolygon.GetBounds();

        float xSize = b.size.x / (nFillPolygons + 0.0f);
        fillPolygons = new Polygon[nFillPolygons];
        fillPolygonFlags = new bool[nFillPolygons];
        for (int i = 0; i < nFillPolygons; i++)
        {
            fillPolygonFlags[i] = false;

            Polygon fillPolygon = Polygon.CreatePolygon(4, outlinePolygon.transform);
            fillPolygon.SetColor(Color.red);
            fillPolygon.transform.localScale = new Vector3(xSize / fillPolygon.GetBounds().size.x, .1f, 0.0f);
            pos = fillPolygon.transform.localPosition;
            pos.z = DLayer;
            pos.x = b.size.x / 2.0f - (i + .5f) * fillPolygon.GetBounds().size.x;
            fillPolygon.transform.localPosition = pos;

            fillPolygons[i] = fillPolygon;
        }
    }


    void Update()
    {
        Vector2 p = Camera.main.ScreenToWorldPoint(Input.mousePosition + new Vector3(0, 0, 1));
        if (active && squarePolygon.PointNearPolygon(p, .2f, .1f))
        {
            for (int i = 0; i < fillPolygons.Length; i++)
            {
                if (fillPolygonFlags[i])
                    continue;
                Polygon poly = fillPolygons[i];
                if (poly.PointNearPolygon(p, .2f, .1f))
                {
                    fillPolygonFlags[i] = true;
                    poly.SetColor(Color.green);
                }
            }

            if (MinigameCompleted())
            {
                completed = true;
                success = true;
            }
        }

        if (active)
        {
            timer -= Time.deltaTime;
            if (timer <= 0)
            {
                completed = true;
                success = false;
                textCounter.text = "00.00";
            }
            else
            {
                textCounter.text = string.Format("{0:00.00}", timer);
            }
        }

        if (completed)
        {
            active = false;
            textInfo.text = success ? "Success!" : "Failure.";
            StartCoroutine(FadeOut());
        }
    }

    public IEnumerator FadeOut()
    {
        if (success)
        {
            checkMark.gameObject.SetActive(true);
        }
        else
        {
            crossMark.gameObject.SetActive(true);
        }

        MeshRenderer outlingMr = polygonHolder.transform.Find("Outline").gameObject.GetComponent<MeshRenderer>();
        while (outlingMr.material.color.a > 0)
        {
            Color c = outlingMr.material.color;
            c.a -= .07f;
            outlingMr.material.color = c;

            foreach(Polygon poly in fillPolygons) {
                MeshRenderer pMr = poly.GetComponent<MeshRenderer>();
                Color polyc = pMr.material.color;
                polyc.a -= 0.7f;
                pMr.material.color = polyc;
            }

            MeshRenderer mr = squarePolygon.GetComponent<MeshRenderer>();
            c = mr.material.color;
            c.a -= success ? .1f : .07f;
            mr.material.color = c;

            yield return new WaitForSeconds(.1f);
        }

        polygonHolder.SetActive(false);

    }

    bool MinigameCompleted()
    {
        foreach (bool b in fillPolygonFlags)
        {
            if (!b)
                return false;
        }
        return true;
    }

    void OnGUI()
    {
        if (GUI.Button(new Rect(10, 10, Screen.width * .2f, Screen.height * .1f), "Reset"))
            Application.LoadLevel(Application.loadedLevel);

    }

}
