﻿using UnityEngine;
using System.Collections;

public class PrimitiveBoard : MonoBehaviour {

    void Awake() {
        Generate();
    }

    void Generate() {
        /* We are a game object
         * --
         * want subobjects that are hexagons
         * -create gameobject & add it to the world
         * -attach the script
         * -fdsa
         */ 

        Color[] colors = { Color.red, Color.white, Color.blue };

        GameObject ob = new GameObject();//Instantiate(new GameObject(), transform) as GameObject;
        ob.transform.parent = transform;
        Polygon polygon = ob.AddComponent<Polygon>();
        polygon.SetQuality(6);
        Bounds bounds = polygon.GetBounds();
        float x = bounds.extents.magnitude;

        polygon.SetColor(Color.red);

        ob = new GameObject();//Instantiate(new GameObject(), transform) as GameObject;
        ob.transform.parent = transform;
        ob.transform.localPosition = new Vector3(bounds.size.x * 1.5f, 0.0f, 0.0f);
        polygon = ob.AddComponent<Polygon>();
        polygon.SetQuality(6);

        polygon.SetColor(Color.white);

        ob = new GameObject();//Instantiate(new GameObject(), transform) as GameObject;
        ob.transform.parent = transform;
        ob.transform.localPosition = new Vector3(bounds.size.x * .75f, bounds.size.y * .5f, 0.0f);
        polygon = ob.AddComponent<Polygon>();
        polygon.SetQuality(6);

        polygon.SetColor(Color.blue);
    }
}
