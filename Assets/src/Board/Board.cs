﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Board : MonoBehaviour {
	public enum BoardState { Idle, Busy};

	public BoardSection boardSectionPrefab;

	public float offsetXInPixels;
	public float offsetYInPixels;
	public int tileWidth;
	public int widthInTiles;
	public int tileHeight;
	public int heightInTiles;

	public CharacterPanel characterPanel;

	public string collisionString;

	public RogueTacticsGui mGUI;

	public int[,] collision;
	public BoardSection[,] boardSections;
	// Used for when the board is highlighted for move, attack heal etc...
	List<BoardSection> boardStateNodes;

	public BoardState boardState;
	public BoardSection activeBoardSection;

	public Party currentParty;
	public Party playerParty;
	public Party enemyParty;

	public GameObject abilityPanel;

	private RogueTacticsGui gui;

	private GameControl gameControl;

	private List<Character> deadPlayerCharacters;
	private List<Character> deadEnemyCharacters;

	private bool gameHasEnded;

	// Use this for initialization
	void Start () {
		deadPlayerCharacters = new List<Character> ();
		deadEnemyCharacters = new List<Character> ();

		gameControl = GameObject.FindGameObjectWithTag ("MainCamera").GetComponent<GameControl> ();
		gui = GameObject.FindGameObjectWithTag ("RogueTacticsGui").GetComponent<RogueTacticsGui> ();

		boardStateNodes = new List<BoardSection> ();

		LoadCollisionString ();
		CreateBoardSections ();

		this.playerParty = GetComponent<PartyFactory> ().generatePlayerParty ();

		this.enemyParty = GetComponent<PartyFactory> ().GenerateAIParty ();

		GameObject.FindGameObjectWithTag("RogueTacticsGui").GetComponent<RogueTacticsGui>().SetPlayerCharacterMembers (this.playerParty.members);
		//characterPanel.SetEnemyCharacterMembers (this.enemyParty.members);

		//LoadCharacterToParty ("Fighter 01", Character.CharacterType.EnemyNonPlayerCharacter, enemyParty);
		PopulatePartyToTiles (playerParty);
		PopulatePartyToTiles (enemyParty);
		enemyParty.ScaleToPartyWithCoefficient (playerParty, 1.2f);
		boardState = BoardState.Idle;

		gameHasEnded = false;

		EndTurn ();
	}


		
/*
 * Loading of board
 */
	void LoadCollisionString() {
		string[] newLines = collisionString.Split (';');
		string[] splits = newLines [0].Split (',');
		collision = new int[newLines.Length, splits.Length];
		for (int i = 0; i < newLines.Length; i++) {
			splits = newLines [i].Split(',');
			for (int j = 0; j < splits.Length; j++) {
				collision [i, j] = int.Parse (splits [j].Trim());
			}
		}
	}

	void CreateBoardSections() {
		boardSections = new BoardSection[heightInTiles, widthInTiles];
		float pixelsPerUnit = 1.0f/100.0f;
		for (int i = 0; i < heightInTiles; i++) {
			for (int j = 0; j < widthInTiles; j++) {
				
				float x = offsetXInPixels * pixelsPerUnit + GetComponent<SpriteRenderer>().bounds.min.x + j * tileWidth * pixelsPerUnit;
				float y = (-offsetYInPixels * pixelsPerUnit) + GetComponent<SpriteRenderer>().bounds.max.y - i * tileHeight * pixelsPerUnit;
				BoardSection go = Instantiate (boardSectionPrefab, new Vector3 (x, y, 0), Quaternion.identity) as BoardSection;
				go.row = i;
				go.col = j;
				go.board = this;
				go.gui = GameObject.FindGameObjectWithTag ("RogueTacticsGui").GetComponent<RogueTacticsGui> ();
				//go.GetComponent<SpriteRenderer> ().enabled = false;

				if (collision [i, j] == 0)
					continue;
				boardSections [i,j] = go;
			}
		}
	}

	void LoadCharacterToParty(string currentCharacter, Character.CharacterType characterType, Party party) {
		if (currentCharacter.Contains ("Fighter")) {
			GameObject go = Instantiate(Resources.Load ("characters/fighters/" + currentCharacter)) as GameObject;
			go.GetComponent<Character> ().characterType = characterType;
			go.GetComponent<Character> ().characterClass = Character.CharacterClass.Fighter;
			party.members.Add (go.GetComponent<Character> ());
		} else if (currentCharacter.Contains ("Ranger")) {
			GameObject go = Instantiate(Resources.Load ("characters/rangers/" + currentCharacter)) as GameObject;
			go.GetComponent<Character> ().characterType = characterType;
			go.GetComponent<Character> ().characterClass = Character.CharacterClass.Ranger;
			party.members.Add (go.GetComponent<Character> ());
		} else if (currentCharacter.Contains ("Mage")) {
			GameObject go = Instantiate(Resources.Load ("characters/mages/" + currentCharacter)) as GameObject;
			go.GetComponent<Character> ().characterType = characterType;
			go.GetComponent<Character> ().characterClass = Character.CharacterClass.Mage;
			party.members.Add (go.GetComponent<Character> ());
		} else if (currentCharacter.Contains ("Ghost")) {
			GameObject go = Instantiate(Resources.Load ("characters/monsters/" + currentCharacter)) as GameObject;
			go.GetComponent<Character> ().characterType = characterType;
			go.GetComponent<Character> ().characterClass = Character.CharacterClass.Fighter;
			party.members.Add (go.GetComponent<Character> ());
		}
	}

	void PopulatePartyToTiles(Party party) {
		ArrayList possiblePartySections;
		if (party.type == Party.PartyType.Enemy) {
			possiblePartySections = GetAvailableEnemySections ();
		} else if (party.type == Party.PartyType.Player) {
			possiblePartySections = GetAvailablePlayerSections ();
		} else {
			possiblePartySections = new ArrayList ();
		}
			
		foreach (Character c in party.members) {
			BoardSection section = (BoardSection)possiblePartySections [0];
			AssignCharacterToSection (c, section);
			possiblePartySections.Remove (section);
		}

		foreach (BoardSection sec in possiblePartySections) {
			int num = 2;
			if (party.type == Party.PartyType.Player) {
				num = 2;
			} else if (party.type == Party.PartyType.Enemy) {
				num = 3;
			} else {
				num = 4;
			}

			if (sec.getCharacter () == null) {
				collision [sec.row, sec.col] = 1;
			} else {
				collision [sec.row, sec.col] = num;
			}
		}
	}

/*
 * Getters
 */

	public string getBoardString() {
		string s = "";
		for (int i = 0; i <= collision.GetUpperBound (0); i++) {
			for (int j = 0; j <= collision.GetUpperBound (1); j++) {
				s += "" + collision [i, j];
			}
		}
		print (s);
		return s;
	}

	ArrayList GetAvailableEnemySections() {
		ArrayList sections = new ArrayList ();
		for (int r = 0; r < collision.GetUpperBound(0); r++) {
			for (int c = 0; c < collision.GetUpperBound(1); c++) {
				if (collision [r, c] == 2) {
					//friendly

				} else if (collision [r, c] == 3) {
					//enemy
					sections.Add(boardSections[r, c]);
				}
			}
		}
		return sections;
	}

	ArrayList GetAvailablePlayerSections() {
		ArrayList sections = new ArrayList ();
		for (int r = 0; r < collision.GetUpperBound(0); r++) {
			for (int c = 0; c < collision.GetUpperBound(1); c++) {
				if (collision [r, c] == 2) {
					//friendly
					sections.Add(boardSections[r, c]);
				} else if (collision [r, c] == 3) {
					//enemy

				}
			}
		}
		return sections;
	}

/*
 * Setters
 */
	public void setDeadCharacterFriendly(Character character) 
	{
		deadPlayerCharacters.Add (character);
	}

	public void setDeadCharacterEnemy(Character character)
	{
		deadEnemyCharacters.Add (character);
	}

	void AssignCharacterToSection(Character c, BoardSection section) {
		section.setCharacter (c, false);
	}

	// Update is called once per frame
	void Update () {
		if (!gameHasEnded) {
			if (playerParty != null && playerParty.members.Count == 0) {
				gui.ShowDefeatScreen ();
				gameHasEnded = true;
			} else if (enemyParty != null && enemyParty.members.Count == 0) {
				gui.ShowVictoryScreen ();
				gameHasEnded = true;
			}

		}
	}
		
/*
 * State methods
 */

	// an eligible board section was clicked on, highlight surrounding tiles, and set their state properly
	public List<BoardSection> SetActiveStateForSection(BoardSection section, bool isCPU) {
		AbilityPanel abilityPanel = GameObject.FindGameObjectWithTag ("AbilityPanel").GetComponent<AbilityPanel> ();
		if (!abilityPanel.isShown)
			abilityPanel.flipButtonHit ();

		gui.setCharacterForAbilityPanel (section.getCharacter());

		int row = section.row;
		int col = section.col;	

		this.boardState = BoardState.Busy;
		this.activeBoardSection = section;

		Character character = section.getCharacter();
		List<BoardSection> tiles = new List<BoardSection> ();

		// cases
		// 1 move/attack loop
		// 2 special ability
		if (character.ap >= 1) {
			List<NPoint> movePts = character.GetMoveSections (collision, row, col);
			foreach (NPoint p in movePts) {

				if (!FooBaz.isInBounds (collision, p.r, p.c)) {
					print ("ERR: SetActiveStateForSection: Point returned is out of range: (" + p.r + ", " + p.c + ")");
					continue;
				}

 				if (boardSections [p.r, p.c] == null) {
					print ("ERR: SetActiveStateForSection: board section null at : (" + p.r + ", " + p.c + ")");
				}

				boardSections [p.r, p.c].AddState (BoardSection.BoardSectionState.Move);
				tiles.Add (boardSections [p.r, p.c]);
			}
		}

		if (character.ap >= 1) {
			List<NPoint> atkPts = character.GetAttackSections (collision, row, col);
			foreach (NPoint p in atkPts) {
				boardSections [p.r, p.c].AddState (BoardSection.BoardSectionState.Attack);
				tiles.Add (boardSections [p.r, p.c]);
			}
		}
		boardStateNodes = tiles;
		if (isCPU)
			return tiles;
		else
			return null;
	}

	BoardSection ActivateTileForAttack(int range, int row, int col, bool isCPU) {
		if (row >= heightInTiles || col >= widthInTiles || row < 0 || col < 0 || boardSections [row, col] == null)
			return null;
		
		BoardSection section = boardSections [row, col];
		if (isCPU)
			return section;

		int dist = ManhattanDistance (row, col, activeBoardSection.row, activeBoardSection.col);
		switch (activeBoardSection.getCharacter().characterClass) {
		case Character.CharacterClass.Fighter:
			if (dist == 1) {
				boardSections [row, col].attack.SetActive (true);
				boardSections [row, col].boardSectionState = BoardSection.BoardSectionState.Attack;
				boardStateNodes.Add (boardSections [row, col]);
			}
			break;
		case Character.CharacterClass.Mage:
			if (dist == 1) {
				boardSections [row, col].attack.SetActive (true);
				boardSections [row, col].boardSectionState = BoardSection.BoardSectionState.Attack;
				boardStateNodes.Add (boardSections [row, col]);
			}
			break;
		case Character.CharacterClass.Ranger:
			if (dist <= 2) {
				boardSections [row, col].attack.SetActive (true);
				boardSections [row, col].boardSectionState = BoardSection.BoardSectionState.Attack;
				boardStateNodes.Add (boardSections [row, col]);
			}
			break;
		}

		if (section.getCharacter() != null) {
			Character character = section.getCharacter();

			// the tile highlights another friendly unit, check if we can heal
			if (character.characterType == Character.CharacterType.PlayerCharacter) {

			} else if (character.characterType == Character.CharacterType.EnemyNonPlayerCharacter) {
				boardSections [row, col].attack.SetActive (true);
				//boardSections [row, col].boardSectionState = BoardSection.BoardSectionState.Attack;
				boardStateNodes.Add (boardSections [row, col]);
			// the tile highlights a neutral character
			} else if (character.characterType == Character.CharacterType.NuetralNonPlayerCharacter) {
			
			}
		} else {
				boardSections [row, col].move.SetActive (true);
				boardSections [row, col].boardSectionState = BoardSection.BoardSectionState.Move;
				boardStateNodes.Add (boardSections [row, col]);
		}
		return null;
	}

	public void DropBoardState() {
		gui.dropState ();
		activeBoardSection = null;
		//activeBoardSection = null;
		boardState = BoardState.Idle;
		for (int i = 0; i < boardStateNodes.Count; i++) {
			BoardSection sec = boardStateNodes [i] as BoardSection;
			sec.DropState ();
		}
		boardStateNodes.Clear ();
	}


	/*
	 * 
	 * 
	 * 
	 * Ability Methods
	 * 
	 * 
	 */
		
	public void ShowAbilityRange(BoardSection section) {
		DropBoardState ();
		this.activeBoardSection = section;
		Ability ability = section.getCharacter().activeAbility;

		List<NPoint> pts;
		pts = FooBaz.GetManhattanRangeForBoard(collision, section.row, section.col, ability.range);
		FooBaz.TrimListAccordingToBoard (pts, collision);
	
		foreach (NPoint pt in pts) {
			BoardSection sec = boardSections [pt.r, pt.c];
			if (ability.type == Ability.AbilityType.Immunity) {
				sec.boardSectionState = BoardSection.BoardSectionState.MagicSelection;
			} else {
				sec.boardSectionState = BoardSection.BoardSectionState.MagicRange;
			}
			sec.attack.SetActive (true);
			boardStateNodes.Add (sec);
		}

		this.boardState = BoardState.Busy;


	}

	public void ShowAbilityAreaOfEffect(BoardSection section) {
		Ability ability = this.activeBoardSection.getCharacter().activeAbility;
		BoardSection activeSection = this.activeBoardSection;
		DropBoardState();
		this.activeBoardSection = activeSection;

		List<NPoint> pts;
		pts = FooBaz.GetManhattanRangeForBoard(collision, section.row, section.col, ability.aoe);

		foreach (NPoint pt in pts) {
			BoardSection sec = boardSections [pt.r, pt.c];
			sec.boardSectionState = BoardSection.BoardSectionState.MagicSelection;
			sec.attack.SetActive (true);
			boardStateNodes.Add (sec);
		}

		this.boardState = BoardState.Busy;
	}

	Vector3 abilityDamageTextDirection;

	public void UseAbilityHere(BoardSection section) {
		Character character = this.activeBoardSection.getCharacter();
		float degree = Random.Range (0, 361);
		abilityDamageTextDirection = new Vector3 (Mathf.Cos (degree), Mathf.Sin (degree), 0);
		foreach (BoardSection sec in boardStateNodes) {
			GameObject go = Instantiate (Resources.Load (character.activeAbility.animationPrefab)) as GameObject;
			go.transform.position = sec.transform.position;

			FireblastAnimation anim = go.GetComponent<FireblastAnimation> ();
			anim.ability = character.activeAbility;
			anim.board = this;
			anim.section = sec;

			if (character.activeAbility.type == Ability.AbilityType.Immunity) {
				go.transform.position = new Vector3 (go.transform.position.x, go.transform.position.y, -3);
			}
		}

		DropBoardState ();
	}

	public void finishUsingAbilityHere(BoardSection section, Ability ability) {
		GameObject go = Instantiate (Resources.Load ("DamageText")) as GameObject;
		DamageText dText = go.GetComponent<DamageText> ();
		dText.manual = true;
		dText.direction = abilityDamageTextDirection;

		section.applyAbilityHere (ability, dText);
	}

	/*
	 * 
	 * 
	 * Move methods
	 * 
	 * 
	 */

	public void MoveToMe(BoardSection section) {
		if (activeBoardSection == section || section == null || section.getCharacter() != null)
			return;
		List <NPoint> pts = FooBaz.findPath (collision, activeBoardSection.getCharacter().GetMoveSections(collision, activeBoardSection.row, activeBoardSection.col), 
												new NPoint (activeBoardSection.row, activeBoardSection.col), 
												new NPoint (section.row, section.col));
		if (pts == null) {
			print ("ERR: MoveToMeForAttack: Couldn't find a path for animating the character");
			// qucik fix
		} else {
			Character c = activeBoardSection.removeCharacter ();
			c.ap--;
			c.GetComponent<CharacterAnimator> ().setPts (pts);
			section.setCharacter (c, true);
			boardState = BoardState.Idle;
			DropBoardState ();
		}
	}


	// pre: active board section set
	public NPoint MoveWithinAttackRangeOfMe(BoardSection section) {
		Character car = activeBoardSection.getCharacter();
		List<NPoint> atkPts = car.GetAttackSections(collision, activeBoardSection.row, activeBoardSection.col);
		atkPts = atkPts.OrderBy (p => -ManhattanDistance (activeBoardSection.row, activeBoardSection.col, p.r, p.c)).ToList();

		List<NPoint> mvPts = car.GetMoveSections (collision, activeBoardSection.row, activeBoardSection.col);

		List<NPoint> possibleMoveLocations = new List<NPoint> ();
		NPoint attackerPos = new NPoint (activeBoardSection.row, activeBoardSection.col);
		NPoint defenderPos = new NPoint (section.row, section.col);
		for (int i = 0; i < atkPts.Count; i++) {
			NPoint p = atkPts [i];
			int offset_row = p.r - activeBoardSection.row;
			int offset_col = p.c - activeBoardSection.col;

			int tr = section.row - p.r;
			int tc = section.col - p.c;

			int r = tr + attackerPos.r;
			int c = tc + attackerPos.c;
			NPoint candidatePt = new NPoint (r, c);
			if (mvPts.Contains (candidatePt) && FooBaz.areIndeciesInRange(collision, r, c)) {
				possibleMoveLocations.Add (candidatePt);
			}
		}

		possibleMoveLocations = possibleMoveLocations.OrderBy (p => ManhattanDistance (activeBoardSection.row, activeBoardSection.col, p.r, p.c)).ToList();
		if (possibleMoveLocations.Count == 0)
			return null;
		return possibleMoveLocations [0];
	}
		
/*
 * 
 * 
 * Attack methods
 * 
 * 
 * 
 */

	public void AttackMe(BoardSection section) {
		Character attackingCharacter = activeBoardSection.getCharacter();
		int dist = ManhattanDistance (section.row, section.col, activeBoardSection.row, activeBoardSection.col);

		DropBoardState ();

		playerParty.Update ();
		enemyParty.Update ();

		if (!attackingCharacter.CanAttackAtDistance (dist))
			return;

		// TODO attack the character
		Character characterToAttack = section.getCharacter();
		bool isDead = section.getCharacter().applyDamage(attackingCharacter.attack);

		if (isDead) {
			section.removeCharacter ();
		}

		GameObject go = Instantiate (Resources.Load ("Slash")) as GameObject;
		go.transform.position = section.transform.position;
		go.transform.position = new Vector3 (go.transform.position.x, go.transform.position.y, -3);

		// character attacking drops ap to 0
		attackingCharacter.ap = 0;
	}

	public void MoveToMeForAttack(BoardSection move, BoardSection attack) 
	{
		if (activeBoardSection == move || move == null || move.getCharacter() != null)
			return;
		List <NPoint> pts = FooBaz.findPath (collision, activeBoardSection.getCharacter().GetMoveSections(collision, activeBoardSection.row, activeBoardSection.col), 
			new NPoint (activeBoardSection.row, activeBoardSection.col), 
			new NPoint (move.row, move.col));

		if (pts == null) {
			print ("ERR: MoveToMeForAttack: Couldn't find a path for animating the character");
		} else {
			Character c = activeBoardSection.removeCharacter ();
			c.ap--;
			c.GetComponent<CharacterAnimator> ().setPts (pts);
			c.GetComponent<CharacterAnimator> ().setIntendedAttackTarget (attack);
				move.setCharacter(c, true);
			boardState = BoardState.Idle;
			DropBoardState ();
		}
	}

	public void finishMoveToMeForAttack(BoardSection section) {
		this.SetActiveStateForSection (section, false);
		this.AttackMe (section);
	}

	public int ManhattanDistance(int r1, int c1, int r2, int c2) {
		return (int)(Mathf.Abs (r1 - r2) + Mathf.Abs (c1 - c2));
	}

	public void EndTurn() {
        gui.hidePauseMenu();
		if (currentParty)
			currentParty.EndTurn ();

		DropBoardState ();
		if (currentParty == playerParty) {
			currentParty = enemyParty;
		} else if (currentParty == enemyParty) {
			currentParty = playerParty;
		} else {
			currentParty = playerParty;
		}

		foreach (BoardSection s in boardSections) {
			if (s == null) {
				continue;
			}
			s.removeWalkState ();
		}

		if (currentParty)
			currentParty.StartTurn ();
	}

	public List<Character> getDeadPlayerCharacters() 
	{
		return deadPlayerCharacters;
	}

	public List<Character> getDeadEnemyCharacters()
	{
		return deadEnemyCharacters;
	}

		
}
