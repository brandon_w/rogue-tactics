﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class BoardSection : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler {

	public const float DRAG_THRESHOLD = .02f;

	public enum BoardSectionState{None, Attack, Heal, Move, Selection, MagicRange, MagicSelection, MoveOrAttack};

	public int row, col;
	private Character character;
	public RogueTacticsGui gui;

	public Board board;

	public GameObject attack, heal, move, selection, friendlyUnitIndicator, enemyUnitIndicator, neutralUnitIndicator;
	public GameObject burn, immune, stun;

	// temporary to see which tiles are occupied by characters
	//public GameObject occupiedByCharacter;

	public BoardSectionState boardSectionState;

	// Use this for initialization
	void Awake () {
		attack	  = transform.Find ("Attack").gameObject; attack.SetActive (false);
		heal 	  = transform.Find ("Heal").gameObject; heal.SetActive (false);
		move 	  = transform.Find ("Move").gameObject; move.SetActive (false);
		selection = transform.Find ("Selection").gameObject; selection.SetActive (false);
		friendlyUnitIndicator = transform.Find ("Friendly Unit Indicator").gameObject; friendlyUnitIndicator.SetActive (false);
		enemyUnitIndicator = transform.Find ("Enemy Unit Indicator").gameObject; enemyUnitIndicator.SetActive (false);
		neutralUnitIndicator = transform.Find ("Neutral Unit Indicator").gameObject; neutralUnitIndicator.SetActive (false);

		burn.SetActive (false);
		immune.SetActive (false);
		stun.SetActive (false);

		transform.parent = GameObject.FindGameObjectWithTag ("Board").transform;
	}
	
	// Update is called once per frame
	void Update () {

	}

	public void setCharacter(Character character, bool animated) {
		this.character = character;
		if (character != null) {
			if (character.party.type == Party.PartyType.Player) {
				board.collision [row, col] = 2;
			} else if (character.party.type == Party.PartyType.Enemy) {
				board.collision [row, col] = 3;
			} else if (character.party.type == Party.PartyType.Neutral) {
				board.collision [row, col] = 4;
			}
		
			character.setBoardSection (this);
			if (!animated) {
				character.transform.position = this.transform.position;
				character.transform.position = new Vector3 (character.transform.position.x, character.transform.position.y, -1);
			}
		}

		// highlight friendly/enemy/neutral
		if (character != null && character.characterType == Character.CharacterType.PlayerCharacter) {
			friendlyUnitIndicator.SetActive (true);
			enemyUnitIndicator.SetActive (false);
			neutralUnitIndicator.SetActive (false);
		} else if (character != null && character.characterType == Character.CharacterType.EnemyNonPlayerCharacter) {
			friendlyUnitIndicator.SetActive (false);
			enemyUnitIndicator.SetActive (true);
			neutralUnitIndicator.SetActive (false);
		} else if (character != null && character.characterType == Character.CharacterType.NuetralNonPlayerCharacter) {
			friendlyUnitIndicator.SetActive (false);
			enemyUnitIndicator.SetActive (false);
			neutralUnitIndicator.SetActive (true);
		} else {
			friendlyUnitIndicator.SetActive (false);
			enemyUnitIndicator.SetActive (false);
			neutralUnitIndicator.SetActive (false);
		}
		//effect icons
		bool isburn = false;
		bool isstun = false;
		bool isImmunue = false;
		if (character != null) {

			foreach (AbilityEffect ae in character.abilityEffects) {
				if (ae.effectType == AbilityEffect.EffectType.Burn) {
					isburn = true;

				} else if (ae.effectType == AbilityEffect.EffectType.ImmuneToDamage) {
					isImmunue = true;
					stun.SetActive (true);
				} else if (ae.effectType == AbilityEffect.EffectType.Stun) {
					isstun = true;
					immune.SetActive (true);
				}
			}
		}

		if (isburn)
			burn.SetActive (true);
		else
			burn.SetActive (false);

		if (isstun)
			stun.SetActive (true);
		else
			stun.SetActive (false);

		if (isImmunue)
			immune.SetActive (true);
		else
			immune.SetActive (false);
	}

	public Character removeCharacter() 
	{
		board.collision [row, col] = 1;
		Character character = this.character;
		this.character.setBoardSection (null);
		//this.character = null;
		setCharacter (null, false);
		if (character == null) {
			friendlyUnitIndicator.SetActive (false);
			enemyUnitIndicator.SetActive (false);
			immune.SetActive (false);
			stun.SetActive (false);
			burn.SetActive (false);
		}



		return character;
	}

	public Character getCharacter()
	{
		return character;
	}

	public void DoShowCharacterActionPanel () {
		if (character == null) {
			return;
		}
		GameObject go = Instantiate (Resources.Load ("Ability Panel")) as GameObject;
		board.abilityPanel = go;
		go.transform.position = new Vector3(this.transform.position.x, this.transform.position.y - go.GetComponent<Renderer>().bounds.size.y / 1.5f, -5);
		//go.GetComponent<AbilityPanel> ().Setup (this, character.activeAbility);
		mouseDown = false;
	}

	public void DropState() {
		attack.SetActive (false);
		heal.SetActive (false);
		move.SetActive (false);
		selection.SetActive (false);
		boardSectionState = BoardSectionState.None;
	}

	public void removeWalkState() {
		move.SetActive (false);
	}

	public void AddState(BoardSectionState boardSectionState) {
		//DropState ();
		if (boardSectionState == BoardSectionState.Move && this.boardSectionState == BoardSectionState.Attack) {
			this.boardSectionState = BoardSectionState.MoveOrAttack;
		} else if(this.boardSectionState == BoardSectionState.Move && boardSectionState == BoardSectionState.Attack) {
			this.boardSectionState = BoardSectionState.MoveOrAttack;
		} else {
			this.boardSectionState = boardSectionState;
		}
		switch (boardSectionState) {
		case BoardSectionState.Attack:
			attack.SetActive (true);
			break;
		case BoardSectionState.Heal:
			heal.SetActive (true);
			break;
		case BoardSectionState.Move:
			move.SetActive (true);
			break;
		case BoardSectionState.Selection:
			selection.SetActive (true);
			break;
		case BoardSectionState.MagicSelection:
			attack.SetActive (true);
			break;
		case BoardSectionState.MagicRange:
			attack.SetActive (true);
			break;
		}
	}

	public void applyAbilityHere(Ability ability, DamageText damageText) {
		damageText.gameObject.transform.position = this.gameObject.transform.position;
		damageText.gameObject.transform.position += new Vector3 (0, 0, -2);

		if (ability.damage == 0) {
			Destroy (damageText.gameObject);
		}

		if (this.character != null) {
			this.character.applyAbility (ability);
		}

		setCharacter (this.character, false);
	}

	/**
	 *	Mouse events 
	 */
	bool mouseDragging = false;
	bool mouseDown = false;
	float prevMouseX, prevMouseY;



	void OnMouseUpAsButton() {
//		if (!mouseDown)
//			return;
//		if (EventSystem.current.IsPointerOverGameObject ())
//			return;
//		mouseDown = false;
//		if (!mouseDragging) {
//			if (boardSectionState == BoardSectionState.MagicRange) {
//				board.ShowAbilityAreaOfEffect (this);
//			} else if (boardSectionState == BoardSectionState.MagicSelection) {
//				board.UseAbilityHere (this);
//			}else if (character != null) {
//				if (board.boardState == Board.BoardState.Idle && character.characterType == Character.CharacterType.PlayerCharacter && character.ap > 0) {
//					board.SetActiveStateForSection (this, false);
//				} else if (board.boardState == Board.BoardState.Busy) {
//					if (boardSectionState == BoardSectionState.Attack || boardSectionState == BoardSectionState.MoveOrAttack){
//						if (!FooBaz.isSameTeam (this.character, board.activeBoardSection.character)) {
//							board.AttackMe (this);
//						}
//					} else if (boardSectionState == BoardSectionState.Move) {
//						NPoint p = board.MoveWithinAttackRangeOfMe(this);
//						if (p != null) {
//							board.MoveToMeForAttack(board.boardSections [p.r, p.c], this);
//						}
//					}
//				} else if (board.boardState == Board.BoardState.Idle && this == board.activeBoardSection) {
//					board.DropBoardState ();
//				}
//			} else {
//				if (board.boardState == Board.BoardState.Busy && (boardSectionState == BoardSectionState.Move || boardSectionState == BoardSectionState.MoveOrAttack)) {
//					board.MoveToMe (this);
//					//character.transform.position = this.transform.position;
//					board.DropBoardState ();
//				} else {
//					board.DropBoardState ();
//				}
//			}
//		}


	}

	#region IPointerUpHandler implementation
	public void OnPointerUp (PointerEventData eventData)
	{
		if (!mouseDragging && mouseDown) {
			if (boardSectionState == BoardSectionState.MagicRange) {
				board.ShowAbilityAreaOfEffect (this);
			} else if (boardSectionState == BoardSectionState.MagicSelection) {
				board.UseAbilityHere (this);
			}else if (character != null) {
				if (board.boardState == Board.BoardState.Idle && character.characterType == Character.CharacterType.PlayerCharacter && character.ap > 0) {
					board.SetActiveStateForSection (this, false);
				} else if (board.boardState == Board.BoardState.Busy) {
					// this is us
					if (this == board.activeBoardSection)
						return;
					if (boardSectionState == BoardSectionState.Attack || boardSectionState == BoardSectionState.MoveOrAttack){
						if (!FooBaz.isSameTeam (this.character, board.activeBoardSection.character)) {
							board.AttackMe (this);
						}
					} else if (boardSectionState == BoardSectionState.Move) {
						NPoint p = board.MoveWithinAttackRangeOfMe(this);
						if (p != null) {
							board.MoveToMeForAttack(board.boardSections [p.r, p.c], this);
						}
					}
				} else if (board.boardState == Board.BoardState.Idle && this == board.activeBoardSection) {
					board.DropBoardState ();
				}
			} else {
				if (board.boardState == Board.BoardState.Busy && (boardSectionState == BoardSectionState.Move || boardSectionState == BoardSectionState.MoveOrAttack)) {
					board.MoveToMe (this);
					//character.transform.position = this.transform.position;
					board.DropBoardState ();
				} else {
					board.DropBoardState ();
				}
			}
		}

		mouseDown = false;
		mouseDragging = false;
	}
	#endregion


	void OnMouseUp() {
//		mouseDown = false;
//		mouseDragging = false;
	}


	#region IPointerDownHandler implementation

	public void OnPointerDown (PointerEventData eventData)
	{
		if (board.mGUI.paused) {
			board.mGUI.hidePauseMenu ();
		}

		mouseDown = true;
		mouseDragging = false;

		prevMouseX = Input.mousePosition.x;
		prevMouseY = Input.mousePosition.y;
	}

	#endregion

	void OnMouseDown() {
//		mouseDown = true;
//		mouseDragging = false;
//
//		prevMouseX = Input.mousePosition.x;
//		prevMouseY = Input.mousePosition.y;
	}

//	void OnMouseDrag() {
//		Vector3 prevPoint = Camera.main.ScreenToWorldPoint(new Vector3(prevMouseX, prevMouseY, 0));
//		Vector3 diff = (prevPoint - Camera.main.ScreenToWorldPoint (Input.mousePosition));
//
//		if (diff.magnitude > DRAG_THRESHOLD) {
//			mouseDragging = true;
//		}
//	}

	#region IDragHandler implementation

	public void OnDrag (PointerEventData eventData)
	{
		Vector3 prevPoint = Camera.main.ScreenToWorldPoint(new Vector3(prevMouseX, prevMouseY, 0));
		Vector3 diff = (prevPoint - Camera.main.ScreenToWorldPoint (Input.mousePosition));

		if (diff.magnitude > DRAG_THRESHOLD) {
			mouseDragging = true;
		}
	}

	#endregion
}
