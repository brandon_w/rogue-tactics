﻿using UnityEngine;
using System.Collections;


public class Ability : MonoBehaviour {
	public enum AbilityType{Fireblast, Stun, Immunity};

	public int range;
	public int aoe;
	public AbilityEffect abilityEffect;
	public AbilityType type;
	public string iconPrefab;
	public string animationPrefab;
	public float damage;

	// Use this for initialization
	void Start () {
		abilityEffect = GetComponent<AbilityEffect> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
