﻿using UnityEngine;
using System.Collections;

public class FireblastAnimation : MonoBehaviour {

	public Ability ability;
	public BoardSection section;
	public Board board;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void animationFinished() {
		board.finishUsingAbilityHere (section, ability);
		Destroy (gameObject);
	}
}
