﻿using UnityEngine;
using System.Collections;

public class AbilityEffect : MonoBehaviour {

	public enum EffectType
	{
		Burn,
		Freeze,
		Stun,
		ImmuneToDamage
	}

	public int turnDuration;
	public EffectType effectType;
	public Character character;
	public Ability ability;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public AbilityEffect ApplyEffect() {
		if (--turnDuration < 0) {
			return this;
		}

		switch (effectType) {
		case EffectType.Burn:
			character.applyDamage (ability.damage / 4);
			break;
		case EffectType.Freeze:
			break;
		case EffectType.Stun:
			character.currentMoveRange = 0;
			break;
		case EffectType.ImmuneToDamage:
			character.immuneToDamage = true;
			break;
		}
		return null;
	}
}
