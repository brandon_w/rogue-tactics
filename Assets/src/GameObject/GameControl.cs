﻿using UnityEngine;
using System.Collections;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine.SceneManagement;

public class GameControl : MonoBehaviour {

	public static GameControl control;

	public Party playerParty;
	public Character character;

	public bool modelOnly;

	void Awake () {
			GameControl.GetPlayerParty ();

			/*for (int i = 0; i < playerParty.members.Count; i++) {
				Character c = playerParty.members [i] as Character;
				Destroy (c.gameObject.GetComponent<CharacterAnimator> ());
			}*/
	}

	// Game Control will always be on the main camera
	public static Party GetPlayerParty() {
		return null;
		//PlayerPrefs.DeleteAll ();
		//Party playerParty = Party.CreatePlayerPartyFromSave ();
//		Party playerParty = null;
//		if (playerParty == null) {
//
//			GameObject go = Instantiate (Resources.Load ("Party")) as GameObject;
//			playerParty = go.GetComponent<Party> ();
//			playerParty.SetBoard (GameObject.FindGameObjectWithTag ("Board") != null ? GameObject.FindGameObjectWithTag ("Board").GetComponent<Board> () : null);
//			playerParty.type = Party.PartyType.Player;
//
//			LoadCharacterToParty ("Fighter 01", Character.CharacterType.PlayerCharacter, playerParty);
//			LoadCharacterToParty ("Fighter 01", Character.CharacterType.PlayerCharacter, playerParty);
//
//			LoadCharacterToParty ("Fighter 01", Character.CharacterType.PlayerCharacter, playerParty);
//
//			LoadCharacterToParty ("Fighter 01", Character.CharacterType.PlayerCharacter, playerParty);
//			LoadCharacterToParty ("Fighter 01", Character.CharacterType.PlayerCharacter, playerParty);
//			LoadCharacterToParty ("Fighter 01", Character.CharacterType.PlayerCharacter, playerParty);
//
//			//LoadCharacterToParty ("Fighter 02", Character.CharacterType.PlayerCharacter, playerParty);
//			//LoadCharacterToParty ("Mage 01", Character.CharacterType.PlayerCharacter, playerParty);
//			//LoadCharacterToParty ("Ranger 01", Character.CharacterType.PlayerCharacter, playerParty);
//			//LoadCharacterToParty ("Ghost 01", Character.CharacterType.PlayerCharacter, playerParty);
//		}
//		playerParty.gameObject.name = "Player Party 1";
//		Camera.main.GetComponent<GameControl> ().playerParty = playerParty;
//		return playerParty;
	}

	
	// Update is called once per frame
	void Update () {
	
	}

	public void Save() {
		BinaryFormatter bf = new BinaryFormatter ();
		FileStream file = File.Open (Application.persistentDataPath + "/playerInfo.dat", FileMode.Open);

		bf.Serialize (file, playerParty);
		file.Close ();
	}

	public void Load() {
		if (File.Exists(Application.persistentDataPath + "/playerInfo.dat")){
			BinaryFormatter bf = new BinaryFormatter ();
			FileStream file = File.Open (Application.persistentDataPath + "/playerInfo.dat", FileMode.Open);

			Party playerParty = (Party)bf.Deserialize (file);
			this.playerParty = playerParty;
		}
	}

	static void LoadCharacterToParty(string currentCharacter, Character.CharacterType characterType, Party party) {
		Character c = null;
		if (currentCharacter.Contains ("Fighter")) {
			String path = "characters/fighters/" + currentCharacter;
			c = Character.CreateCharacterFromPath (path);
			c.party = party;
			c.characterType = characterType;
			c.characterClass = Character.CharacterClass.Fighter;
			party.members.Add (c);
		} else if (currentCharacter.Contains ("Ranger")) {
			String path = "characters/rangers/" + currentCharacter;
			c = Character.CreateCharacterFromPath (path);
			c.party = party;
			c.characterType = characterType;
			c.characterClass = Character.CharacterClass.Ranger;
			party.members.Add (c);
		} else if (currentCharacter.Contains ("Mage")) {
			String path =  "characters/mages/" + currentCharacter;
			c = Character.CreateCharacterFromPath (path);
			c.party = party;
			c.characterType = characterType;
			c.characterClass = Character.CharacterClass.Mage;
			party.members.Add (c);
		} else if (currentCharacter.Contains ("Ghost")) {
			String path =  "characters/monsters/" + currentCharacter;
			c = Character.CreateCharacterFromPath (path);
			c.party = party;
			c.characterType = characterType;
			c.characterClass = Character.CharacterClass.Fighter;
			party.members.Add (c);
		}
		c.transform.SetParent (party.gameObject.transform);
	}

	public void NavigateToLevel(String level) {
		//this.playerParty.SavePartyString ();
        SceneManager.LoadScene(level);
		//Application.LoadLevel (level);
	}
}
