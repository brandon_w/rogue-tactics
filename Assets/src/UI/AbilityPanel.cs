﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AbilityPanel : MonoBehaviour {
	public Animator animator;
	public Button collapseButton;
	public bool isShown;

	void Start() {
		animator = GetComponent<Animator> ();
		isShown = animator.GetBool ("isShown");
	}

	public void flipButton() {
		collapseButton.GetComponent<RectTransform> ().Rotate (new Vector3 (0, 0, 180));
	}

	public void flipButtonHit() {
		isShown = !isShown;
		animator.SetBool ("isShown", isShown);
	}
}