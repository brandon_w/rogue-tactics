﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CharacterImage : MonoBehaviour {

	Sprite sprite;
	Image image;

	public static CharacterImage createCharacterImage(Sprite sprite) {
		GameObject characterImage = new GameObject ("Character Image");
		characterImage.AddComponent<CharacterImage> ();
		characterImage.GetComponent<CharacterImage> ().sprite = sprite;
		characterImage.AddComponent<RectTransform> ();
		return characterImage.GetComponent<CharacterImage> ();
	}

	// Use this for initialization
	void Start () {
		gameObject.transform.localScale = new Vector3 (.25f, .25f, .25f);

		image = gameObject.AddComponent<Image> ();
		image.sprite = sprite;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
