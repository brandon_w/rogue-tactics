﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class HubGUI : MonoBehaviour {

	string[] maps = { "realdev", "Map"};

	public bool isActive = false;

	GameControl gameControl;

	public Text partyCapacity;

	//public Button addFighterButton, addMageButton, addRangerButton;
	public Text currentPartyFighterCount, currentPartyMageCount, currentPartyRangerCount;

	public GameObject characterSelectPanel, characterSelectCurrentPartyPanel;
	// Use this for initialization
	void Start () {
		characterSelectPanel.SetActive (false);

		gameControl = GameObject.FindGameObjectWithTag ("MainCamera").GetComponent<GameControl> ();
		//activateCharacterSelectPanel ();

		updateCharacterPanelNumbers ();
	}

	public int addUpCharacters() {
		return PlayerPrefs.GetInt ("Fighters") + PlayerPrefs.GetInt("Mages") + PlayerPrefs.GetInt("Rangers");
	}

	public void updateCharacterPanelNumbers() {
		partyCapacity.text = addUpCharacters() + " / 6";

		currentPartyFighterCount.text = PlayerPrefs.GetInt ("Fighters") + "";
		currentPartyMageCount.text = PlayerPrefs.GetInt ("Mages") + "";
		currentPartyRangerCount.text = PlayerPrefs.GetInt ("Rangers") + "";
	}

	public void startRandomMap() {
		System.Random rand = new System.Random ();
		string map = maps[Random.Range (0, maps.Length)];
		Application.LoadLevel (map);
	}

	public void activateCharacterSelectPanel() {
		isActive = true;
		characterSelectPanel.SetActive (true);
	}

	public void deactivateCharacterSelectPanel() {
		isActive = false;
		characterSelectPanel.SetActive (false);
	}

	public void addFighter() {
		if (addUpCharacters () >= 6) {
			return;
		}

		PlayerPrefs.SetInt ("Fighters", PlayerPrefs.GetInt ("Fighters") + 1);

		updateCharacterPanelNumbers ();
	}

	public void addMage() {
		if (addUpCharacters () >= 6) {
			return;
		}

		PlayerPrefs.SetInt ("Mages", PlayerPrefs.GetInt ("Mages") + 1);

		updateCharacterPanelNumbers ();
	}

	public void addRanger() {
		if (addUpCharacters () >= 6) {
			return;
		}

		PlayerPrefs.SetInt ("Rangers", PlayerPrefs.GetInt ("Rangers") + 1);

		updateCharacterPanelNumbers ();
	}

	public void removeFighter() {
		if (PlayerPrefs.GetInt ("Fighters") <= 0) {
			return;
		}

		PlayerPrefs.SetInt ("Fighters", PlayerPrefs.GetInt ("Fighters") - 1);

		updateCharacterPanelNumbers ();
	}

	public void removeMage() {
		if (PlayerPrefs.GetInt ("Mages") <= 0) {
			return;
		}

		PlayerPrefs.SetInt ("Mages", PlayerPrefs.GetInt ("Mages") - 1);

		updateCharacterPanelNumbers ();
	}

	public void removeRanger() {
		if (PlayerPrefs.GetInt ("Rangers") <= 0) {
			return;
		}

		PlayerPrefs.SetInt ("Rangers", PlayerPrefs.GetInt ("Rangers") - 1);

		updateCharacterPanelNumbers ();
	}

}
