﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CharacterPanel : MonoBehaviour {
	public ArrayList playerMembers, enemyMembers;

	private SpriteRenderer sr;

	public Camera uiCamera;

	public Animator animator;
	public Button collapseButton;
	public bool isShown;

	void Start() {
		animator = GetComponent<Animator> ();
		isShown = animator.GetBool ("isShown");
	}

	public void flipButton() {
		collapseButton.GetComponent<RectTransform> ().Rotate (new Vector3 (0, 0, 180));
	}

	public void flipButtonHit() {
		isShown = !isShown;
		animator.SetBool ("isShown", isShown);
	}

	// Use this for initialization
/*	void Start () {
		this.sr = GetComponent<SpriteRenderer> ();

		prevOrthoSize = Camera.main.orthographicSize;
	}

	Vector3 prevCameraPos;
	float prevOrthoSize;
	bool flag = true;
	// Update is called once per frame
	void Update () {
		if (playerMembers != null)
			UpdatePlayerCharacterPositions ();
		if (enemyMembers != null)
			UpdateEnemyCharacterPositions ();
	}

	public void SetPlayerCharacterMembers(ArrayList members) {
		this.playerMembers = members;
	}

	public void SetEnemyCharacterMembers(ArrayList members) {
		this.enemyMembers = members;
	}
		
	public void UpdatePlayerCharacterPositions() {
		SpriteRenderer r = GetComponent<SpriteRenderer> ();
		float left = r.bounds.min.x;
		float step = r.bounds.size.x / (playerMembers.Count + 1);
		float y = r.bounds.min.y + r.bounds.size.y * 2 / 3;
		for (int i = 0; i < playerMembers.Count; i++) {
			Character c = ((Character)playerMembers [i]);
			GameObject go = c.icon;

			if (go.gameObject.transform.parent != gameObject.transform)
				go.gameObject.transform.parent = gameObject.transform;

			go.SetActive (true);

			float x = left + (i + 1) * step;
			go.transform.position = new Vector3 (x, y, -5);

			go.GetComponent<ImmuneToCameraZoom> ().camera = uiCamera;
		}
	}

	public void UpdateEnemyCharacterPositions() {
		SpriteRenderer r = GetComponent<SpriteRenderer> ();
		float left = r.bounds.min.x;
		float step = r.bounds.size.x / (playerMembers.Count + 1);
		float y = r.bounds.min.y + r.bounds.size.y * 2 / 3;
		for (int i = 0; i < enemyMembers.Count; i++) {
			Character c = ((Character)enemyMembers [i]);
			GameObject go = c.icon.gameObject;

			if (go.gameObject.transform.parent != gameObject.transform)
				go.gameObject.transform.parent = gameObject.transform;

			go.SetActive (true);

			float x = left + (i + 1) * step;
			go.transform.position = new Vector3 (x, y, -5);

			go.GetComponent<ImmuneToCameraZoom> ().camera = uiCamera;
		}
	}

	void OnMouseUpAsButton() {
		Vector3 pos = uiCamera.ScreenToWorldPoint(Input.mousePosition);

		foreach (Character go in playerMembers) {
			Bounds bounds = go.icon.GetComponent<SpriteRenderer> ().bounds;
			if (pointInside (pos, bounds)) {
				Board b = GameObject.FindGameObjectWithTag ("Board").GetComponent<Board> ();
				b.DropBoardState();
				b.SetActiveStateForSection (go.getBoardSection(), false);
			}
		}
	}

	private bool pointInside(Vector3 point, Bounds rect) {
		return rect.min.x < point.x && rect.min.y < point.y && rect.max.x > point.x && rect.max.y > point.y;
	}*/
}
