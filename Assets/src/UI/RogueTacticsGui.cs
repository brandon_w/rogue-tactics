﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;
public class RogueTacticsGui : MonoBehaviour {

	public Animator actionPanelAnimator;
	public Board board;
	public GameObject inspectionPanel, characterPanel, abilityPanel, victory, defeat, menu;
	public Text hpText, apText, attackText, defenseText, agilityText;

	public Text abilityDescription;
	public GameObject abilityDescriptionHolder;

	public List<Character> playerMembers, enemyMembers;

	float pendingMenuPositionX;

	private bool isUp;

	public bool paused = false;

	// Use this for initialization
	void Start () {
		isUp = false;
		inspectionPanel.SetActive (false);
		//characterPanel.SetActive (false);
		victory.SetActive (false);
		defeat.SetActive (false);
		abilityDescriptionHolder.SetActive(false);

		pendingMenuPositionX = menu.GetComponent<RectTransform> ().transform.localPosition.x;
	}

	void Update() {
		RectTransform pauseRT = menu.GetComponent<RectTransform> ();
		pauseRT.anchoredPosition = new Vector2 (Mathf.Lerp(pauseRT.anchoredPosition.x, pendingMenuPositionX, Time.deltaTime * 10), pauseRT.anchoredPosition.y);
	}

	public void setCharacterForAbilityPanel(Character character) {
		int nAbilities = 1;
		Ability ability = character.activeAbility;

		GameObject go = Instantiate (Resources.Load (ability.iconPrefab)) as GameObject;
		go.transform.SetParent (abilityPanel.transform, false);
		go.GetComponent<Button>().onClick.AddListener(() => {
			clickAbility(character);
		});
	}

	public void clickAbility(Character character) {
		if (paused) {
			hidePauseMenu ();
		}

		Board b = GameObject.FindGameObjectWithTag ("Board").GetComponent<Board> ();
		b.ShowAbilityRange (character.getBoardSection ());

		Ability ability = character.activeAbility;

		abilityDescriptionHolder.SetActive(true);
		if (ability.type == Ability.AbilityType.Fireblast) {
			abilityDescription.text = "Fire ability that does damage in an area. Applies Burn that does Damage over time.";
		} else if (ability.type == Ability.AbilityType.Immunity) {
			abilityDescription.text = "Provides immunity for one turn. Prevents all damage on the immune character.";
		} else if (ability.type == Ability.AbilityType.Stun) {
			abilityDescription.text = "Stuns the target, making it unable to take an action next turn.";
		}
	}

	public void dropState() {
		abilityDescriptionHolder.SetActive(false);

		List<GameObject> children = new List<GameObject> ();
		foreach (Transform child in abilityPanel.transform) {
			children.Add (child.gameObject);
		}
		children.ForEach (child => Destroy (child));
	}

	public void SetPlayerCharacterMembers(List<Character> members) {
		this.playerMembers = members;

		RectTransform rt = characterPanel.GetComponent<RectTransform> ();
		//float spacing = (rt.rect.width - 45.0f *( members.Count + 0.0f)) / (members.Count + 0.0f);
		//if (spacing < 0) {
		//	spacing = 0;
		//}
		//characterPanel.GetComponent<HorizontalLayoutGroup> ().spacing = spacing;
		for (int i = 0; i < playerMembers.Count; i++) {
			Character c = ((Character)playerMembers [i]);
			GameObject go = c.icon;
			go.GetComponent<CharacterIcon> ().characterGuid = c.guid;
			go.SetActive (true);
			go.transform.SetParent (characterPanel.gameObject.transform, false);

			go.GetComponent<Button>().onClick.AddListener(() => { 
				ClickCharacter(c);
			}); 
		}
		//Canvas.ForceUpdateCanvases ();
	}

	public void ClickCharacter (Character c) {
		if (paused) {
			hidePauseMenu ();
		}

		Board b = GameObject.FindGameObjectWithTag ("Board").GetComponent<Board> ();
		b.DropBoardState();
		b.SetActiveStateForSection (c.getBoardSection(), false);
	}

	public void SetEnemyCharacterMembers(List<Character> members) {
		this.enemyMembers = members;
	}


	public void MouseIsAtSection(BoardSection section) {
		if (section == null && inspectionPanel.activeInHierarchy) {
			characterPanel.SetActive (true);
			inspectionPanel.SetActive (false);
		} else if (section != null && section.getCharacter() != null && !inspectionPanel.activeInHierarchy){
			characterPanel.SetActive (false);
			inspectionPanel.SetActive (true);

			hpText.text = (int)section.getCharacter().hp + " / " + (int)section.getCharacter().totalHp;
			apText.text = (int)section.getCharacter().ap + " / " + (int)section.getCharacter().totalAp;
			attackText.text = "" + (int)section.getCharacter().attack;
			defenseText.text = "" + (int)section.getCharacter().defense;
			agilityText.text = "" + (int)section.getCharacter().agility;
		}
	}

	public void ShowVictoryScreen() {
        hidePauseMenu();
		PlayerPrefsUtil.incrementVictoryCount ();
		victory.SetActive (true);

		StartCoroutine (populateDeadEnemyCharactersToVictoryPanel ());
		StartCoroutine (populateDeadPlayerCharactersToVictoryPanel ());
	}

	public void ShowDefeatScreen() {
        hidePauseMenu();
		PlayerPrefsUtil.incrementDefeatCount ();
		defeat.SetActive(true);

		StartCoroutine (populateDeadPlayerCharactersToDefeatPanel ());
		StartCoroutine (populateDeadEnemyCharactersToDefeatPanel ());
	}

	public void Continue() {
		Camera.main.GetComponent<GameControl> ().NavigateToLevel ("mainmenu");
	}
	public void GoBackToMap() {
		//Camera.main.GetComponent<GameControl> ().NavigateToLevel ("home");

	}

	public void goToMainMenu()
	{
		Camera.main.GetComponent<GameControl> ().NavigateToLevel ("mainmenu");
	
		//victory.SetActive (true);

		//StartCoroutine (populateDeadEnemyCharactersToVictoryPanel ());
		//StartCoroutine (populateDeadPlayerCharactersToVictoryPanel ());
	}


	public void GoAbilities() {

	}

	public void GoItems() {

	}

	public void hideCharacterPanel()
	{

	}

	public void showPauseMenu() {
		paused = true;

		pendingMenuPositionX = 65;


	}

	public void hidePauseMenu() {
		paused = false;

		pendingMenuPositionX = -65;
	}

	public IEnumerator populateDeadPlayerCharactersToVictoryPanel() {
		List<Character> charactersLost = board.playerParty.members;

		int count = 0;
		float currentPadding = 0.0f;
		foreach (Character character in charactersLost) {
			if (count > 0)
				yield return new WaitForSeconds (.3f);
			else
				yield return new WaitForSeconds (.1f);
			Sprite sprite = character.GetComponent<SpriteRenderer> ().sprite;

			CharacterImage characterImage = CharacterImage.createCharacterImage (sprite);
			RectTransform characterTransform = characterImage.GetComponent<RectTransform> ();
			RectTransform panelTransform = victory.transform.FindChild ("Units Lost").GetComponent<RectTransform>();

			characterImage.transform.SetParent (panelTransform);
			currentPadding += characterTransform.rect.width / 8.0f;
			Vector3 difference = new Vector3(currentPadding - panelTransform.rect.width/2.0f + 10.0f, 0.0f, 0.0f);
			characterTransform.localPosition = difference;

			count++;
		}
	}

	public IEnumerator populateDeadEnemyCharactersToVictoryPanel() {
		List<Character> charactersSlain = board.enemyParty.members;

		int total = charactersSlain.Count;
		int count = 0;
		float currentPadding = 0.0f;
		foreach (Character character in charactersSlain) {
			if (count > 0)
				yield return new WaitForSeconds (.3f);
			else
				yield return new WaitForSeconds (.1f);
			Sprite sprite = character.GetComponent<SpriteRenderer> ().sprite;

			CharacterImage characterImage = CharacterImage.createCharacterImage (sprite);
			RectTransform characterTransform = characterImage.GetComponent<RectTransform> ();
			RectTransform panelTransform = victory.transform.FindChild ("Units Slain").GetComponent<RectTransform>();

			characterImage.transform.SetParent (panelTransform);
			currentPadding += characterTransform.rect.width / 8.0f;
			Vector3 difference = new Vector3(currentPadding - panelTransform.rect.width/2.0f + 10.0f, 0.0f, 0.0f);
			characterTransform.localPosition = difference;

			count++;
		}
	}

	public IEnumerator populateDeadPlayerCharactersToDefeatPanel() {
		List<Character> charactersLost = board.getDeadPlayerCharacters ();
		List<Character> charactersSlain = board.getDeadEnemyCharacters();

		int total = charactersLost.Count;
		int count = 0;
		float currentPadding = 0.0f;
		foreach (Character character in charactersLost) {
			if (count > 0)
				yield return new WaitForSeconds (.3f);
			else
				yield return new WaitForSeconds (.1f);
			Sprite sprite = character.GetComponent<SpriteRenderer> ().sprite;

			CharacterImage characterImage = CharacterImage.createCharacterImage (sprite);
			RectTransform characterTransform = characterImage.GetComponent<RectTransform> ();
			RectTransform panelTransform = defeat.transform.FindChild ("Units Lost").GetComponent<RectTransform>();

			characterImage.transform.SetParent (panelTransform);
			currentPadding += characterTransform.rect.width / 8.0f;
			Vector3 difference = new Vector3(currentPadding - panelTransform.rect.width/2.0f + 10.0f, 0.0f, 0.0f);
			characterTransform.localPosition = difference;

			count++;
		}
	}

	public IEnumerator populateDeadEnemyCharactersToDefeatPanel() {
		List<Character> charactersSlain = board.getDeadEnemyCharacters();

		int total = charactersSlain.Count;
		int count = 0;
		float currentPadding = 0.0f;
		foreach (Character character in charactersSlain) {
			if (count > 0)
				yield return new WaitForSeconds (.3f);
			else
				yield return new WaitForSeconds (.1f);
			Sprite sprite = character.GetComponent<SpriteRenderer> ().sprite;

			CharacterImage characterImage = CharacterImage.createCharacterImage (sprite);
			RectTransform characterTransform = characterImage.GetComponent<RectTransform> ();
			RectTransform panelTransform = defeat.transform.FindChild ("Units Slain").GetComponent<RectTransform>();

			characterImage.transform.SetParent (panelTransform);
			currentPadding += characterTransform.rect.width / 8.0f;
			Vector3 difference = new Vector3(currentPadding - panelTransform.rect.width/2.0f + 10.0f, 0.0f, 0.0f);
			characterTransform.localPosition = difference;

			count++;
		}
	}

}
