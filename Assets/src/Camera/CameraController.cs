﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {
	public const float DRAG_THRESHOLD = .02f;
	// Use this for initialization
	void Start () {
		hubGUI = GameObject.FindGameObjectWithTag ("HubGUI") != null ? GameObject.FindGameObjectWithTag ("HubGUI").GetComponent<HubGUI>() : null;
	}
	float minSize = .5f;
	float maxSize = 2.5f;
	float touchZoomSensitivity = .005f;
	float mouseZoomSensitivity = .4f;

	Vector2 prevMouse; 
	bool dragging, noPt;

	HubGUI hubGUI;


	void Update()
	{
		if (hubGUI != null && hubGUI.isActive)
			return;
		float mouseScrollWheel = -Input.GetAxis("Mouse ScrollWheel");

		if (mouseScrollWheel != 0.0f) {
			// ... change the orthographic size based on the change in distance between the touches.
			Camera.main.orthographicSize += Mathf.Min(mouseScrollWheel * mouseZoomSensitivity);

			// Make sure the orthographic size never drops below zero.
			Camera.main.orthographicSize = Mathf.Clamp(Camera.main.orthographicSize, minSize, maxSize);
		}

		Vector3 prevPoint;
		Vector3 diff;
		if (Input.touchCount == 1) {
			if (noPt) {
				noPt = false;
				prevMouse = Input.touches[0].position;
			}
			prevPoint = Camera.main.ScreenToWorldPoint (prevMouse);
			diff = (prevPoint - Camera.main.ScreenToWorldPoint (Input.touches [0].position));
		} else {
			prevPoint = Camera.main.ScreenToWorldPoint (prevMouse);
			diff = (prevPoint - Camera.main.ScreenToWorldPoint (Input.mousePosition));
		}
		if (diff.magnitude > DRAG_THRESHOLD) {
			dragging = true;
		}

		// If there are two touches on the device...
		if (Input.touchCount >= 2) {
			// Store both touches.
			Touch touchZero = Input.GetTouch (0);
			Touch touchOne = Input.GetTouch (1);

			// Find the position in the previous frame of each touch.
			Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
			Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

			// Find the magnitude of the vector (the distance) between the touches in each frame.
			float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
			float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

			// Find the difference in the distances between each frame.
			float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;

			// If the camera is orthographic...
			if (Camera.main.orthographic) {
				// ... change the orthographic size based on the change in distance between the touches.
				Camera.main.orthographicSize += Mathf.Min (deltaMagnitudeDiff * touchZoomSensitivity);

				// Make sure the orthographic size never drops below zero.
				Camera.main.orthographicSize = Mathf.Clamp (Camera.main.orthographicSize, minSize, maxSize);
			}
		} else if (Input.touchCount == 1 || Input.GetMouseButton (0)) {
			if (dragging)
				Camera.main.transform.position += diff;
		} else {
			dragging = false;
		}


		if (SystemInfo.deviceType == DeviceType.Desktop) {
			prevMouse = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
		} else {
			if (Input.touchCount == 1) {
				prevMouse = Input.touches[0].position;
			} else {
				noPt = true;
			}
		}

	}

	void OnMouseDown() {

	}

	void OnMouseDrag() {
		dragging = true;

	}
		
}
