﻿using UnityEngine;
using System.Collections;

public class ImmuneToCameraZoom : MonoBehaviour {
	
	private float orthoOrg;
	private float orthoCurr;
	private Vector3 scaleOrg;
	private Vector3 posOrg;
	public new Camera camera;

	// Use this for initialization
	void Start () {
		orthoOrg = camera.orthographicSize;
		orthoCurr = orthoOrg;
		scaleOrg = transform.localScale;
		posOrg = camera.WorldToViewportPoint (transform.position);
	}
	
	// Update is called once per frame
	void LateUpdate () {
		// ----------------------------------
		// maintain scale and position as camera changes
		float osize = camera.orthographicSize;
		if (orthoCurr != osize) {
			transform.localScale = scaleOrg * osize / orthoOrg;
			orthoCurr = osize;
			transform.position = camera.ViewportToWorldPoint (posOrg);
		}

		transform.position = camera.ViewportToWorldPoint (posOrg);
	}
}
