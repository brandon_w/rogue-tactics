﻿using UnityEngine;
using System.Collections;

public class UICameraFollow : MonoBehaviour {
	private Vector3 posOrg;
	public new Camera camera;

	// Use this for initialization
	void Start () {

		posOrg = camera.WorldToViewportPoint (transform.position);
	}

	// Update is called once per frame
	void LateUpdate () {
		transform.position = camera.ViewportToWorldPoint (posOrg);
	}
}
