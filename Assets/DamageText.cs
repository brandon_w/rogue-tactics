﻿using UnityEngine;
using System.Collections;

public class DamageText : MonoBehaviour {

	public float lifetime;
	private float startTime, currentTime;
	public Vector3 direction;
	public float speed;
	public bool manual;

	// Use this for initialization
	void Start () {
		startTime = Time.realtimeSinceStartup;
		if (!manual) {
			float degree = Random.Range (0, 361);
			direction = new Vector3 (Mathf.Cos (degree), Mathf.Sin (degree), 0);
		}
	}
	
	// Update is called once per frame
	void Update () {
		currentTime = Time.realtimeSinceStartup;
		if (currentTime - startTime >= lifetime) {
			Destroy (gameObject);
		}
		transform.position += direction * speed;
	}
}
